﻿namespace Z2Randomizer
{
    partial class MainUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUI));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label32 = new System.Windows.Forms.Label();
            this.startLifeBox = new System.Windows.Forms.ComboBox();
            this.startMagBox = new System.Windows.Forms.ComboBox();
            this.startAtkBox = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.maxHeartsBox = new System.Windows.Forms.ComboBox();
            this.livesBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.techCmbo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.heartCmbo = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.thunderBox = new System.Windows.Forms.CheckBox();
            this.spellBox = new System.Windows.Forms.CheckBox();
            this.reflectBox = new System.Windows.Forms.CheckBox();
            this.fireBox = new System.Windows.Forms.CheckBox();
            this.fairyBox = new System.Windows.Forms.CheckBox();
            this.lifeBox = new System.Windows.Forms.CheckBox();
            this.jumpBox = new System.Windows.Forms.CheckBox();
            this.shieldBox = new System.Windows.Forms.CheckBox();
            this.spellShuffleBox = new System.Windows.Forms.CheckBox();
            this.itemGrp = new System.Windows.Forms.GroupBox();
            this.keyBox = new System.Windows.Forms.CheckBox();
            this.hammerBox = new System.Windows.Forms.CheckBox();
            this.crossBox = new System.Windows.Forms.CheckBox();
            this.fluteBox = new System.Windows.Forms.CheckBox();
            this.bootsBox = new System.Windows.Forms.CheckBox();
            this.raftBox = new System.Windows.Forms.CheckBox();
            this.gloveBox = new System.Windows.Forms.CheckBox();
            this.candleBox = new System.Windows.Forms.CheckBox();
            this.shuffleItemBox = new System.Windows.Forms.CheckBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.baguBox = new System.Windows.Forms.CheckBox();
            this.waterBoots = new System.Windows.Forms.CheckBox();
            this.shuffleHidden = new System.Windows.Forms.CheckBox();
            this.vanillaOriginalTerrain = new System.Windows.Forms.CheckBox();
            this.mazeBiome = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.eastBiome = new System.Windows.Forms.ComboBox();
            this.dmBiome = new System.Windows.Forms.ComboBox();
            this.westBiome = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.boulderConnectionBox = new System.Windows.Forms.CheckBox();
            this.saneCaveShuffleBox = new System.Windows.Forms.CheckBox();
            this.hideLocsBox = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.continentConnectionBox = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.encounterBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.hideKasutoBox = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.hpCmbo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.p7Shuffle = new System.Windows.Forms.CheckBox();
            this.palaceSwapBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.allowPathEnemies = new System.Windows.Forms.CheckBox();
            this.shuffleEncounters = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bossItem = new System.Windows.Forms.CheckBox();
            this.removeTbird = new System.Windows.Forms.CheckBox();
            this.gpBox = new System.Windows.Forms.CheckBox();
            this.upaBox = new System.Windows.Forms.CheckBox();
            this.palacePalette = new System.Windows.Forms.CheckBox();
            this.tbirdBox = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numGemsCbo = new System.Windows.Forms.ComboBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.lifeEffBox = new System.Windows.Forms.ComboBox();
            this.magEffBox = new System.Windows.Forms.ComboBox();
            this.atkEffBox = new System.Windows.Forms.ComboBox();
            this.scaleLevels = new System.Windows.Forms.CheckBox();
            this.label30 = new System.Windows.Forms.Label();
            this.lifeCapBox = new System.Windows.Forms.ComboBox();
            this.magCapBox = new System.Windows.Forms.ComboBox();
            this.atkCapBox = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.expBox = new System.Windows.Forms.GroupBox();
            this.lifeExpNeeded = new System.Windows.Forms.CheckBox();
            this.magicExpNeeded = new System.Windows.Forms.CheckBox();
            this.shuffleAtkExp = new System.Windows.Forms.CheckBox();
            this.shuffleAllExp = new System.Windows.Forms.CheckBox();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.spellEnemy = new System.Windows.Forms.CheckBox();
            this.combineFireBox = new System.Windows.Forms.CheckBox();
            this.disableJarBox = new System.Windows.Forms.CheckBox();
            this.shuffleSpellLocationsBox = new System.Windows.Forms.CheckBox();
            this.lifeRefilBox = new System.Windows.Forms.CheckBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.label31 = new System.Windows.Forms.Label();
            this.expDropBox = new System.Windows.Forms.ComboBox();
            this.shuffleDripper = new System.Windows.Forms.CheckBox();
            this.mixEnemies = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.shufflePalaceEnemies = new System.Windows.Forms.CheckBox();
            this.shuffleOverworldEnemies = new System.Windows.Forms.CheckBox();
            this.swordImmuneBox = new System.Windows.Forms.CheckBox();
            this.stealExpAmt = new System.Windows.Forms.CheckBox();
            this.stealExpBox = new System.Windows.Forms.CheckBox();
            this.shuffleEnemyHPBox = new System.Windows.Forms.CheckBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.shufflePbagExp = new System.Windows.Forms.CheckBox();
            this.spellItemBox = new System.Windows.Forms.CheckBox();
            this.pbagItemShuffleBox = new System.Windows.Forms.CheckBox();
            this.kasutoBox = new System.Windows.Forms.CheckBox();
            this.palaceKeys = new System.Windows.Forms.CheckBox();
            this.shuffleSmallItemsBox = new System.Windows.Forms.CheckBox();
            this.mixItemBox = new System.Windows.Forms.CheckBox();
            this.overworldItemBox = new System.Windows.Forms.CheckBox();
            this.palaceItemBox = new System.Windows.Forms.CheckBox();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.randoDrops = new System.Windows.Forms.CheckBox();
            this.standardDrops = new System.Windows.Forms.CheckBox();
            this.largeKey = new System.Windows.Forms.CheckBox();
            this.large1up = new System.Windows.Forms.CheckBox();
            this.large500 = new System.Windows.Forms.CheckBox();
            this.large200 = new System.Windows.Forms.CheckBox();
            this.large100 = new System.Windows.Forms.CheckBox();
            this.large50 = new System.Windows.Forms.CheckBox();
            this.largeRedJar = new System.Windows.Forms.CheckBox();
            this.largeBlueJar = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.smallKey = new System.Windows.Forms.CheckBox();
            this.small1up = new System.Windows.Forms.CheckBox();
            this.small500 = new System.Windows.Forms.CheckBox();
            this.small200 = new System.Windows.Forms.CheckBox();
            this.small100 = new System.Windows.Forms.CheckBox();
            this.small50 = new System.Windows.Forms.CheckBox();
            this.smallRedJar = new System.Windows.Forms.CheckBox();
            this.smallBlueJar = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.enemyDropBox = new System.Windows.Forms.CheckBox();
            this.pbagDrop = new System.Windows.Forms.CheckBox();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.townSpellHints = new System.Windows.Forms.CheckBox();
            this.spellItemHints = new System.Windows.Forms.CheckBox();
            this.communityBox = new System.Windows.Forms.CheckBox();
            this.helpfulHints = new System.Windows.Forms.CheckBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.flashingOff = new System.Windows.Forms.CheckBox();
            this.upAC1 = new System.Windows.Forms.CheckBox();
            this.beamCmbo = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.shieldColor = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tunicColor = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.spriteCmbo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.disableMusicBox = new System.Windows.Forms.CheckBox();
            this.enemyPalette = new System.Windows.Forms.CheckBox();
            this.beamBox = new System.Windows.Forms.CheckBox();
            this.fastSpellBox = new System.Windows.Forms.CheckBox();
            this.jumpNormalbox = new System.Windows.Forms.CheckBox();
            this.disableLowHealthBeep = new System.Windows.Forms.CheckBox();
            this.fileTextBox = new System.Windows.Forms.TextBox();
            this.seedTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.seedBtn = new System.Windows.Forms.Button();
            this.fileBtn = new System.Windows.Forms.Button();
            this.generateBtn = new System.Windows.Forms.Button();
            this.flagBox = new System.Windows.Forms.TextBox();
            this.updateBtn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.wikiBtn = new System.Windows.Forms.Button();
            this.botBtn = new System.Windows.Forms.Button();
            this.megmetBtn = new System.Windows.Forms.Button();
            this.zoraBtn = new System.Windows.Forms.Button();
            this.wizzBtn = new System.Windows.Forms.Button();
            this.paraBtn = new System.Windows.Forms.Button();
            this.lizBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.customBox1 = new System.Windows.Forms.TextBox();
            this.customBox2 = new System.Windows.Forms.TextBox();
            this.customSave1 = new System.Windows.Forms.Button();
            this.customLoad1 = new System.Windows.Forms.Button();
            this.customBox3 = new System.Windows.Forms.TextBox();
            this.customSave2 = new System.Windows.Forms.Button();
            this.customLoad2 = new System.Windows.Forms.Button();
            this.customSave3 = new System.Windows.Forms.Button();
            this.customLoad3 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.customRooms = new System.Windows.Forms.CheckBox();
            this.blockerBox = new System.Windows.Forms.CheckBox();
            this.bossRoomBox = new System.Windows.Forms.CheckBox();
            this.palaceBox = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.dashBox = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.itemGrp.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.expBox.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 90);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(517, 311);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label32);
            this.tabPage4.Controls.Add(this.startLifeBox);
            this.tabPage4.Controls.Add(this.startMagBox);
            this.tabPage4.Controls.Add(this.startAtkBox);
            this.tabPage4.Controls.Add(this.label33);
            this.tabPage4.Controls.Add(this.label34);
            this.tabPage4.Controls.Add(this.label35);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.maxHeartsBox);
            this.tabPage4.Controls.Add(this.livesBox);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.techCmbo);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Controls.Add(this.heartCmbo);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Controls.Add(this.itemGrp);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(509, 285);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Start Configuration";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(388, 182);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(72, 13);
            this.label32.TabIndex = 28;
            this.label32.Text = "Starting Level";
            // 
            // startLifeBox
            // 
            this.startLifeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.startLifeBox.FormattingEnabled = true;
            this.startLifeBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.startLifeBox.Location = new System.Drawing.Point(443, 201);
            this.startLifeBox.Name = "startLifeBox";
            this.startLifeBox.Size = new System.Drawing.Size(32, 21);
            this.startLifeBox.TabIndex = 27;
            this.toolTip1.SetToolTip(this.startLifeBox, "Starting Life Level");
            // 
            // startMagBox
            // 
            this.startMagBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.startMagBox.FormattingEnabled = true;
            this.startMagBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.startMagBox.Location = new System.Drawing.Point(405, 201);
            this.startMagBox.Name = "startMagBox";
            this.startMagBox.Size = new System.Drawing.Size(32, 21);
            this.startMagBox.TabIndex = 26;
            this.toolTip1.SetToolTip(this.startMagBox, "Starting Magic Level");
            // 
            // startAtkBox
            // 
            this.startAtkBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.startAtkBox.FormattingEnabled = true;
            this.startAtkBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.startAtkBox.Location = new System.Drawing.Point(367, 201);
            this.startAtkBox.Name = "startAtkBox";
            this.startAtkBox.Size = new System.Drawing.Size(32, 21);
            this.startAtkBox.TabIndex = 25;
            this.toolTip1.SetToolTip(this.startAtkBox, "Starting Attack Level");
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(447, 229);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(24, 13);
            this.label33.TabIndex = 24;
            this.label33.Text = "Life";
            this.toolTip1.SetToolTip(this.label33, "Starting Life Level");
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(407, 229);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 13);
            this.label34.TabIndex = 23;
            this.label34.Text = "Mag";
            this.toolTip1.SetToolTip(this.label34, "Starting Magic Level");
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(372, 229);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(23, 13);
            this.label35.TabIndex = 22;
            this.label35.Text = "Atk";
            this.toolTip1.SetToolTip(this.label35, "Starting Attack Level");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(338, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Max Heart Containers";
            // 
            // maxHeartsBox
            // 
            this.maxHeartsBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.maxHeartsBox.FormattingEnabled = true;
            this.maxHeartsBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "Random"});
            this.maxHeartsBox.Location = new System.Drawing.Point(341, 90);
            this.maxHeartsBox.Name = "maxHeartsBox";
            this.maxHeartsBox.Size = new System.Drawing.Size(121, 21);
            this.maxHeartsBox.TabIndex = 18;
            this.toolTip1.SetToolTip(this.maxHeartsBox, "The number of heart containers you start with");
            // 
            // livesBox
            // 
            this.livesBox.AutoSize = true;
            this.livesBox.Location = new System.Drawing.Point(340, 162);
            this.livesBox.Name = "livesBox";
            this.livesBox.Size = new System.Drawing.Size(159, 17);
            this.livesBox.TabIndex = 15;
            this.livesBox.Text = "Randomize Number of Lives";
            this.toolTip1.SetToolTip(this.livesBox, "Start with anywhere from 2-5 lives");
            this.livesBox.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(338, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Starting Techs";
            // 
            // techCmbo
            // 
            this.techCmbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.techCmbo.FormattingEnabled = true;
            this.techCmbo.Items.AddRange(new object[] {
            "None",
            "Downstab",
            "Upstab",
            "Both",
            "Random"});
            this.techCmbo.Location = new System.Drawing.Point(341, 133);
            this.techCmbo.Name = "techCmbo";
            this.techCmbo.Size = new System.Drawing.Size(121, 21);
            this.techCmbo.TabIndex = 13;
            this.toolTip1.SetToolTip(this.techCmbo, "The sword techniques you start with");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(338, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Starting Heart Containers";
            // 
            // heartCmbo
            // 
            this.heartCmbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.heartCmbo.FormattingEnabled = true;
            this.heartCmbo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "Random"});
            this.heartCmbo.Location = new System.Drawing.Point(342, 48);
            this.heartCmbo.Name = "heartCmbo";
            this.heartCmbo.Size = new System.Drawing.Size(121, 21);
            this.heartCmbo.TabIndex = 9;
            this.toolTip1.SetToolTip(this.heartCmbo, "The number of heart containers you start with");
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.thunderBox);
            this.groupBox1.Controls.Add(this.spellBox);
            this.groupBox1.Controls.Add(this.reflectBox);
            this.groupBox1.Controls.Add(this.fireBox);
            this.groupBox1.Controls.Add(this.fairyBox);
            this.groupBox1.Controls.Add(this.lifeBox);
            this.groupBox1.Controls.Add(this.jumpBox);
            this.groupBox1.Controls.Add(this.shieldBox);
            this.groupBox1.Controls.Add(this.spellShuffleBox);
            this.groupBox1.Location = new System.Drawing.Point(175, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(160, 205);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "                                        ";
            // 
            // thunderBox
            // 
            this.thunderBox.AutoSize = true;
            this.thunderBox.Location = new System.Drawing.Point(23, 180);
            this.thunderBox.Name = "thunderBox";
            this.thunderBox.Size = new System.Drawing.Size(116, 17);
            this.thunderBox.TabIndex = 7;
            this.thunderBox.Text = "Start With Thunder";
            this.toolTip1.SetToolTip(this.thunderBox, "Start with thunder spell");
            this.thunderBox.UseVisualStyleBackColor = true;
            // 
            // spellBox
            // 
            this.spellBox.AutoSize = true;
            this.spellBox.Location = new System.Drawing.Point(23, 157);
            this.spellBox.Name = "spellBox";
            this.spellBox.Size = new System.Drawing.Size(99, 17);
            this.spellBox.TabIndex = 6;
            this.spellBox.Text = "Start With Spell";
            this.toolTip1.SetToolTip(this.spellBox, "Start with spell spell");
            this.spellBox.UseVisualStyleBackColor = true;
            // 
            // reflectBox
            // 
            this.reflectBox.AutoSize = true;
            this.reflectBox.Location = new System.Drawing.Point(23, 134);
            this.reflectBox.Name = "reflectBox";
            this.reflectBox.Size = new System.Drawing.Size(110, 17);
            this.reflectBox.TabIndex = 5;
            this.reflectBox.Text = "Start With Reflect";
            this.toolTip1.SetToolTip(this.reflectBox, "Start with reflect spell");
            this.reflectBox.UseVisualStyleBackColor = true;
            // 
            // fireBox
            // 
            this.fireBox.AutoSize = true;
            this.fireBox.Location = new System.Drawing.Point(23, 111);
            this.fireBox.Name = "fireBox";
            this.fireBox.Size = new System.Drawing.Size(93, 17);
            this.fireBox.TabIndex = 4;
            this.fireBox.Text = "Start With Fire";
            this.toolTip1.SetToolTip(this.fireBox, "Start with fire spell");
            this.fireBox.UseVisualStyleBackColor = true;
            // 
            // fairyBox
            // 
            this.fairyBox.AutoSize = true;
            this.fairyBox.Location = new System.Drawing.Point(23, 88);
            this.fairyBox.Name = "fairyBox";
            this.fairyBox.Size = new System.Drawing.Size(98, 17);
            this.fairyBox.TabIndex = 3;
            this.fairyBox.Text = "Start With Fairy";
            this.toolTip1.SetToolTip(this.fairyBox, "Start with fairy spell");
            this.fairyBox.UseVisualStyleBackColor = true;
            // 
            // lifeBox
            // 
            this.lifeBox.AutoSize = true;
            this.lifeBox.Location = new System.Drawing.Point(23, 65);
            this.lifeBox.Name = "lifeBox";
            this.lifeBox.Size = new System.Drawing.Size(93, 17);
            this.lifeBox.TabIndex = 2;
            this.lifeBox.Text = "Start With Life";
            this.toolTip1.SetToolTip(this.lifeBox, "Start with life spell");
            this.lifeBox.UseVisualStyleBackColor = true;
            // 
            // jumpBox
            // 
            this.jumpBox.AutoSize = true;
            this.jumpBox.Location = new System.Drawing.Point(23, 42);
            this.jumpBox.Name = "jumpBox";
            this.jumpBox.Size = new System.Drawing.Size(101, 17);
            this.jumpBox.TabIndex = 1;
            this.jumpBox.Text = "Start With Jump";
            this.toolTip1.SetToolTip(this.jumpBox, "Start with jump spell");
            this.jumpBox.UseVisualStyleBackColor = true;
            // 
            // shieldBox
            // 
            this.shieldBox.AutoSize = true;
            this.shieldBox.Location = new System.Drawing.Point(23, 19);
            this.shieldBox.Name = "shieldBox";
            this.shieldBox.Size = new System.Drawing.Size(105, 17);
            this.shieldBox.TabIndex = 1;
            this.shieldBox.Text = "Start With Shield";
            this.toolTip1.SetToolTip(this.shieldBox, "Start with shield spell");
            this.shieldBox.UseVisualStyleBackColor = true;
            // 
            // spellShuffleBox
            // 
            this.spellShuffleBox.AutoSize = true;
            this.spellShuffleBox.Location = new System.Drawing.Point(6, 0);
            this.spellShuffleBox.Name = "spellShuffleBox";
            this.spellShuffleBox.Size = new System.Drawing.Size(129, 17);
            this.spellShuffleBox.TabIndex = 1;
            this.spellShuffleBox.Text = "Shuffle Starting Spells";
            this.toolTip1.SetToolTip(this.spellShuffleBox, "Each spell has a 25% chance of being known");
            this.spellShuffleBox.UseVisualStyleBackColor = true;
            this.spellShuffleBox.CheckedChanged += new System.EventHandler(this.spellShuffleBox_CheckedChanged);
            // 
            // itemGrp
            // 
            this.itemGrp.Controls.Add(this.keyBox);
            this.itemGrp.Controls.Add(this.hammerBox);
            this.itemGrp.Controls.Add(this.crossBox);
            this.itemGrp.Controls.Add(this.fluteBox);
            this.itemGrp.Controls.Add(this.bootsBox);
            this.itemGrp.Controls.Add(this.raftBox);
            this.itemGrp.Controls.Add(this.gloveBox);
            this.itemGrp.Controls.Add(this.candleBox);
            this.itemGrp.Controls.Add(this.shuffleItemBox);
            this.itemGrp.Location = new System.Drawing.Point(9, 33);
            this.itemGrp.Name = "itemGrp";
            this.itemGrp.Size = new System.Drawing.Size(160, 205);
            this.itemGrp.TabIndex = 0;
            this.itemGrp.TabStop = false;
            this.itemGrp.Text = "                                        ";
            // 
            // keyBox
            // 
            this.keyBox.AutoSize = true;
            this.keyBox.Location = new System.Drawing.Point(23, 180);
            this.keyBox.Name = "keyBox";
            this.keyBox.Size = new System.Drawing.Size(126, 17);
            this.keyBox.TabIndex = 7;
            this.keyBox.Text = "Start With Magic Key";
            this.toolTip1.SetToolTip(this.keyBox, "Start with the magic key");
            this.keyBox.UseVisualStyleBackColor = true;
            // 
            // hammerBox
            // 
            this.hammerBox.AutoSize = true;
            this.hammerBox.Location = new System.Drawing.Point(23, 157);
            this.hammerBox.Name = "hammerBox";
            this.hammerBox.Size = new System.Drawing.Size(115, 17);
            this.hammerBox.TabIndex = 6;
            this.hammerBox.Text = "Start With Hammer";
            this.toolTip1.SetToolTip(this.hammerBox, "Start with the hammer");
            this.hammerBox.UseVisualStyleBackColor = true;
            // 
            // crossBox
            // 
            this.crossBox.AutoSize = true;
            this.crossBox.Location = new System.Drawing.Point(23, 134);
            this.crossBox.Name = "crossBox";
            this.crossBox.Size = new System.Drawing.Size(102, 17);
            this.crossBox.TabIndex = 5;
            this.crossBox.Text = "Start With Cross";
            this.toolTip1.SetToolTip(this.crossBox, "Start with the cross");
            this.crossBox.UseVisualStyleBackColor = true;
            // 
            // fluteBox
            // 
            this.fluteBox.AutoSize = true;
            this.fluteBox.Location = new System.Drawing.Point(23, 111);
            this.fluteBox.Name = "fluteBox";
            this.fluteBox.Size = new System.Drawing.Size(99, 17);
            this.fluteBox.TabIndex = 4;
            this.fluteBox.Text = "Start With Flute";
            this.toolTip1.SetToolTip(this.fluteBox, "Start with the flute");
            this.fluteBox.UseVisualStyleBackColor = true;
            // 
            // bootsBox
            // 
            this.bootsBox.AutoSize = true;
            this.bootsBox.Location = new System.Drawing.Point(23, 88);
            this.bootsBox.Name = "bootsBox";
            this.bootsBox.Size = new System.Drawing.Size(103, 17);
            this.bootsBox.TabIndex = 3;
            this.bootsBox.Text = "Start With Boots";
            this.toolTip1.SetToolTip(this.bootsBox, "Start with the boots");
            this.bootsBox.UseVisualStyleBackColor = true;
            // 
            // raftBox
            // 
            this.raftBox.AutoSize = true;
            this.raftBox.Location = new System.Drawing.Point(23, 65);
            this.raftBox.Name = "raftBox";
            this.raftBox.Size = new System.Drawing.Size(96, 17);
            this.raftBox.TabIndex = 2;
            this.raftBox.Text = "Start With Raft";
            this.toolTip1.SetToolTip(this.raftBox, "Start with the raft");
            this.raftBox.UseVisualStyleBackColor = true;
            // 
            // gloveBox
            // 
            this.gloveBox.AutoSize = true;
            this.gloveBox.Location = new System.Drawing.Point(23, 42);
            this.gloveBox.Name = "gloveBox";
            this.gloveBox.Size = new System.Drawing.Size(104, 17);
            this.gloveBox.TabIndex = 1;
            this.gloveBox.Text = "Start With Glove";
            this.toolTip1.SetToolTip(this.gloveBox, "Start with the glove");
            this.gloveBox.UseVisualStyleBackColor = true;
            // 
            // candleBox
            // 
            this.candleBox.AutoSize = true;
            this.candleBox.Location = new System.Drawing.Point(23, 19);
            this.candleBox.Name = "candleBox";
            this.candleBox.Size = new System.Drawing.Size(109, 17);
            this.candleBox.TabIndex = 1;
            this.candleBox.Text = "Start With Candle";
            this.toolTip1.SetToolTip(this.candleBox, "Start with candle");
            this.candleBox.UseVisualStyleBackColor = true;
            // 
            // shuffleItemBox
            // 
            this.shuffleItemBox.AccessibleDescription = "";
            this.shuffleItemBox.AutoSize = true;
            this.shuffleItemBox.Location = new System.Drawing.Point(6, 0);
            this.shuffleItemBox.Name = "shuffleItemBox";
            this.shuffleItemBox.Size = new System.Drawing.Size(126, 17);
            this.shuffleItemBox.TabIndex = 1;
            this.shuffleItemBox.Text = "Shuffle Starting Items";
            this.toolTip1.SetToolTip(this.shuffleItemBox, "Each item has a 25% chance of being in your inventory");
            this.shuffleItemBox.UseVisualStyleBackColor = true;
            this.shuffleItemBox.CheckedChanged += new System.EventHandler(this.shuffleItemBox_CheckedChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.baguBox);
            this.tabPage1.Controls.Add(this.waterBoots);
            this.tabPage1.Controls.Add(this.shuffleHidden);
            this.tabPage1.Controls.Add(this.vanillaOriginalTerrain);
            this.tabPage1.Controls.Add(this.mazeBiome);
            this.tabPage1.Controls.Add(this.label42);
            this.tabPage1.Controls.Add(this.eastBiome);
            this.tabPage1.Controls.Add(this.dmBiome);
            this.tabPage1.Controls.Add(this.westBiome);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.label40);
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.boulderConnectionBox);
            this.tabPage1.Controls.Add(this.saneCaveShuffleBox);
            this.tabPage1.Controls.Add(this.hideLocsBox);
            this.tabPage1.Controls.Add(this.label37);
            this.tabPage1.Controls.Add(this.continentConnectionBox);
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.encounterBox);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.hideKasutoBox);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.hpCmbo);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.p7Shuffle);
            this.tabPage1.Controls.Add(this.palaceSwapBox);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.allowPathEnemies);
            this.tabPage1.Controls.Add(this.shuffleEncounters);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(509, 285);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Overworld";
            this.tabPage1.ToolTipText = "When selected, will hide Kasuto behind a forest tile";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // baguBox
            // 
            this.baguBox.AutoSize = true;
            this.baguBox.Location = new System.Drawing.Point(242, 99);
            this.baguBox.Name = "baguBox";
            this.baguBox.Size = new System.Drawing.Size(142, 17);
            this.baguBox.TabIndex = 47;
            this.baguBox.Text = "Generate Bagu\'s Woods";
            this.toolTip1.SetToolTip(this.baguBox, "When selected, bagu\'s house will be hidden in a forest surrounded by lost woods t" +
        "iles.");
            this.baguBox.UseVisualStyleBackColor = true;
            // 
            // waterBoots
            // 
            this.waterBoots.AutoSize = true;
            this.waterBoots.Location = new System.Drawing.Point(242, 76);
            this.waterBoots.Name = "waterBoots";
            this.waterBoots.Size = new System.Drawing.Size(179, 17);
            this.waterBoots.TabIndex = 46;
            this.waterBoots.Text = "All Water is Walkable with Boots";
            this.toolTip1.SetToolTip(this.waterBoots, "When selected, all water within the map boundaries can be traversed with the boot" +
        "s.");
            this.waterBoots.UseVisualStyleBackColor = true;
            // 
            // shuffleHidden
            // 
            this.shuffleHidden.AutoSize = true;
            this.shuffleHidden.Location = new System.Drawing.Point(6, 211);
            this.shuffleHidden.Name = "shuffleHidden";
            this.shuffleHidden.Size = new System.Drawing.Size(200, 17);
            this.shuffleHidden.TabIndex = 45;
            this.shuffleHidden.Text = "Shuffle which Location(s) are Hidden";
            this.toolTip1.SetToolTip(this.shuffleHidden, "When selected, shuffles which location are in the hidden palace and hidden kasuto" +
        " spots on the overworld.");
            this.shuffleHidden.UseVisualStyleBackColor = true;
            // 
            // vanillaOriginalTerrain
            // 
            this.vanillaOriginalTerrain.AutoSize = true;
            this.vanillaOriginalTerrain.Location = new System.Drawing.Point(241, 259);
            this.vanillaOriginalTerrain.Name = "vanillaOriginalTerrain";
            this.vanillaOriginalTerrain.Size = new System.Drawing.Size(247, 17);
            this.vanillaOriginalTerrain.TabIndex = 44;
            this.vanillaOriginalTerrain.Text = "Shuffled Vanilla Locations Show Actual Terrain";
            this.toolTip1.SetToolTip(this.vanillaOriginalTerrain, "When selected, if a shuffled vanilla map is in play, the map will show the correc" +
        "t terrain type of each location.");
            this.vanillaOriginalTerrain.UseVisualStyleBackColor = true;
            // 
            // mazeBiome
            // 
            this.mazeBiome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mazeBiome.FormattingEnabled = true;
            this.mazeBiome.Items.AddRange(new object[] {
            "Vanilla",
            "Vanilla (shuffled)",
            "Vanilla-Like",
            "Random (with Vanilla)"});
            this.mazeBiome.Location = new System.Drawing.Point(362, 232);
            this.mazeBiome.Name = "mazeBiome";
            this.mazeBiome.Size = new System.Drawing.Size(121, 21);
            this.mazeBiome.TabIndex = 43;
            this.toolTip1.SetToolTip(this.mazeBiome, "Maze Island overworld map style.");
            this.mazeBiome.SelectedIndexChanged += new System.EventHandler(this.mazeBiome_SelectedIndexChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(239, 235);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(99, 13);
            this.label42.TabIndex = 42;
            this.label42.Text = "Maze Island Biome:";
            this.toolTip1.SetToolTip(this.label42, "Maze Island overworld map style.");
            // 
            // eastBiome
            // 
            this.eastBiome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.eastBiome.FormattingEnabled = true;
            this.eastBiome.Items.AddRange(new object[] {
            "Vanilla",
            "Vanilla (shuffled)",
            "Vanilla-Like",
            "Islands",
            "Canyon",
            "Volcano",
            "Mountainous",
            "Random (no Vanilla)",
            "Random (with Vanilla)"});
            this.eastBiome.Location = new System.Drawing.Point(362, 205);
            this.eastBiome.Name = "eastBiome";
            this.eastBiome.Size = new System.Drawing.Size(121, 21);
            this.eastBiome.TabIndex = 41;
            this.toolTip1.SetToolTip(this.eastBiome, "East Hyrule overworld map style.");
            this.eastBiome.SelectedIndexChanged += new System.EventHandler(this.eastBiome_SelectedIndexChanged);
            // 
            // dmBiome
            // 
            this.dmBiome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dmBiome.FormattingEnabled = true;
            this.dmBiome.Items.AddRange(new object[] {
            "Vanilla",
            "Vanilla (shuffled)",
            "Vanilla-Like",
            "Islands",
            "Canyon",
            "Caldera",
            "Mountainous",
            "Random (no Vanilla)",
            "Random (with Vanilla)"});
            this.dmBiome.Location = new System.Drawing.Point(362, 178);
            this.dmBiome.Name = "dmBiome";
            this.dmBiome.Size = new System.Drawing.Size(121, 21);
            this.dmBiome.TabIndex = 40;
            this.toolTip1.SetToolTip(this.dmBiome, "Death Mountain overworld map style.");
            this.dmBiome.SelectedIndexChanged += new System.EventHandler(this.dmBiome_SelectedIndexChanged);
            // 
            // westBiome
            // 
            this.westBiome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.westBiome.FormattingEnabled = true;
            this.westBiome.Items.AddRange(new object[] {
            "Vanilla",
            "Vanilla (shuffled)",
            "Vanilla-Like",
            "Islands",
            "Canyon",
            "Caldera",
            "Mountainous",
            "Random (no Vanilla)",
            "Random (with Vanilla)"});
            this.westBiome.Location = new System.Drawing.Point(362, 152);
            this.westBiome.Name = "westBiome";
            this.westBiome.Size = new System.Drawing.Size(121, 21);
            this.westBiome.TabIndex = 39;
            this.toolTip1.SetToolTip(this.westBiome, "West Hyrule overworld map style.");
            this.westBiome.SelectedIndexChanged += new System.EventHandler(this.westBiome_SelectedIndexChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(239, 208);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(111, 13);
            this.label41.TabIndex = 37;
            this.label41.Text = "East Continent Biome:";
            this.toolTip1.SetToolTip(this.label41, "East Hyrule overworld map style.");
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(239, 181);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(118, 13);
            this.label40.TabIndex = 36;
            this.label40.Text = "Death Mountain Biome:";
            this.toolTip1.SetToolTip(this.label40, "Death Mountain overworld map style.");
            // 
            // label39
            // 
            this.label39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label39.Location = new System.Drawing.Point(241, 147);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(255, 2);
            this.label39.TabIndex = 35;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(239, 155);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(115, 13);
            this.label38.TabIndex = 34;
            this.label38.Text = "West Continent Biome:";
            this.toolTip1.SetToolTip(this.label38, "West Hyrule overworld map style.");
            // 
            // boulderConnectionBox
            // 
            this.boulderConnectionBox.AutoSize = true;
            this.boulderConnectionBox.Location = new System.Drawing.Point(242, 51);
            this.boulderConnectionBox.Name = "boulderConnectionBox";
            this.boulderConnectionBox.Size = new System.Drawing.Size(249, 17);
            this.boulderConnectionBox.TabIndex = 33;
            this.boulderConnectionBox.Text = "Allow Connection Caves to be Boulder Blocked";
            this.toolTip1.SetToolTip(this.boulderConnectionBox, "When selected, allows boulders to block any cave.");
            this.boulderConnectionBox.UseVisualStyleBackColor = true;
            // 
            // saneCaveShuffleBox
            // 
            this.saneCaveShuffleBox.AutoSize = true;
            this.saneCaveShuffleBox.Location = new System.Drawing.Point(242, 28);
            this.saneCaveShuffleBox.Name = "saneCaveShuffleBox";
            this.saneCaveShuffleBox.Size = new System.Drawing.Size(183, 17);
            this.saneCaveShuffleBox.TabIndex = 32;
            this.saneCaveShuffleBox.Text = "Restrict Connection Cave Shuffle";
            this.toolTip1.SetToolTip(this.saneCaveShuffleBox, "When selected, caves will be placed in a more logical manner in which they \"point" +
        "\" at their destination.");
            this.saneCaveShuffleBox.UseVisualStyleBackColor = true;
            // 
            // hideLocsBox
            // 
            this.hideLocsBox.AutoSize = true;
            this.hideLocsBox.Location = new System.Drawing.Point(242, 6);
            this.hideLocsBox.Name = "hideLocsBox";
            this.hideLocsBox.Size = new System.Drawing.Size(169, 17);
            this.hideLocsBox.TabIndex = 31;
            this.hideLocsBox.Text = "Hide Less Important Locations";
            this.toolTip1.SetToolTip(this.hideLocsBox, "When selected, blends unimportant locations in with the surrounding terrain.");
            this.hideLocsBox.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(239, 125);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(117, 13);
            this.label37.TabIndex = 30;
            this.label37.Text = "Continent Connections:";
            this.toolTip1.SetToolTip(this.label37, "Modes for how the different continents can connect to each other.");
            // 
            // continentConnectionBox
            // 
            this.continentConnectionBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.continentConnectionBox.FormattingEnabled = true;
            this.continentConnectionBox.Items.AddRange(new object[] {
            "Normal",
            "R+B Border Shuffle",
            "Transportation Shuffle",
            "Anything Goes"});
            this.continentConnectionBox.Location = new System.Drawing.Point(362, 122);
            this.continentConnectionBox.Name = "continentConnectionBox";
            this.continentConnectionBox.Size = new System.Drawing.Size(121, 21);
            this.continentConnectionBox.TabIndex = 29;
            this.toolTip1.SetToolTip(this.continentConnectionBox, "Modes for how the different continents can connect to each other.");
            // 
            // label36
            // 
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label36.Location = new System.Drawing.Point(6, 141);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(208, 1);
            this.label36.TabIndex = 28;
            // 
            // encounterBox
            // 
            this.encounterBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.encounterBox.FormattingEnabled = true;
            this.encounterBox.Items.AddRange(new object[] {
            "Normal",
            "50%",
            "None"});
            this.encounterBox.Location = new System.Drawing.Point(93, 114);
            this.encounterBox.Name = "encounterBox";
            this.encounterBox.Size = new System.Drawing.Size(86, 21);
            this.encounterBox.TabIndex = 27;
            this.toolTip1.SetToolTip(this.encounterBox, "Allows you to reduce the encounter rate or turn encounters off entirely.");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 117);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Encounter Rate:";
            this.toolTip1.SetToolTip(this.label13, "Allows you to reduce the encounter rate or turn encounters off entirely.");
            // 
            // hideKasutoBox
            // 
            this.hideKasutoBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hideKasutoBox.FormattingEnabled = true;
            this.hideKasutoBox.Items.AddRange(new object[] {
            "Off",
            "On",
            "Random"});
            this.hideKasutoBox.Location = new System.Drawing.Point(93, 176);
            this.hideKasutoBox.Name = "hideKasutoBox";
            this.hideKasutoBox.Size = new System.Drawing.Size(86, 21);
            this.hideKasutoBox.TabIndex = 25;
            this.toolTip1.SetToolTip(this.hideKasutoBox, "When selected, will hide Kasuto behind a forest tile");
            this.hideKasutoBox.SelectedIndexChanged += new System.EventHandler(this.hideKasutoBox_SelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 178);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Hidden Kasuto:";
            // 
            // hpCmbo
            // 
            this.hpCmbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hpCmbo.FormattingEnabled = true;
            this.hpCmbo.Items.AddRange(new object[] {
            "Off",
            "On",
            "Random"});
            this.hpCmbo.Location = new System.Drawing.Point(93, 149);
            this.hpCmbo.Name = "hpCmbo";
            this.hpCmbo.Size = new System.Drawing.Size(86, 21);
            this.hpCmbo.TabIndex = 23;
            this.toolTip1.SetToolTip(this.hpCmbo, "When selected, will include three eye rock on the overworld");
            this.hpCmbo.SelectedIndexChanged += new System.EventHandler(this.hpCmbo_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 152);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Hidden Palace:";
            // 
            // p7Shuffle
            // 
            this.p7Shuffle.AutoSize = true;
            this.p7Shuffle.Location = new System.Drawing.Point(6, 28);
            this.p7Shuffle.Name = "p7Shuffle";
            this.p7Shuffle.Size = new System.Drawing.Size(173, 17);
            this.p7Shuffle.TabIndex = 21;
            this.p7Shuffle.Text = "Include Great Palace in Shuffle";
            this.toolTip1.SetToolTip(this.p7Shuffle, "When selected, palace 7 does not have to be in the valley of death.");
            this.p7Shuffle.UseVisualStyleBackColor = true;
            // 
            // palaceSwapBox
            // 
            this.palaceSwapBox.AutoSize = true;
            this.palaceSwapBox.Location = new System.Drawing.Point(6, 6);
            this.palaceSwapBox.Name = "palaceSwapBox";
            this.palaceSwapBox.Size = new System.Drawing.Size(187, 17);
            this.palaceSwapBox.TabIndex = 20;
            this.palaceSwapBox.Text = "Allow Palaces to Swap Continents";
            this.toolTip1.SetToolTip(this.palaceSwapBox, "When selected, palaces can move from their normal continents. Palace 1 could be f" +
        "ound on Maze Island or East Hyrule, for example.");
            this.palaceSwapBox.UseVisualStyleBackColor = true;
            this.palaceSwapBox.CheckedChanged += new System.EventHandler(this.palaceSwapBox_CheckedChanged);
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(6, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 1);
            this.label4.TabIndex = 18;
            // 
            // allowPathEnemies
            // 
            this.allowPathEnemies.AutoSize = true;
            this.allowPathEnemies.Location = new System.Drawing.Point(6, 90);
            this.allowPathEnemies.Name = "allowPathEnemies";
            this.allowPathEnemies.Size = new System.Drawing.Size(170, 17);
            this.allowPathEnemies.TabIndex = 15;
            this.allowPathEnemies.Text = "Allow Unsafe Path Encounters";
            this.toolTip1.SetToolTip(this.allowPathEnemies, "If checked, you may have enemies in path encounters");
            this.allowPathEnemies.UseVisualStyleBackColor = true;
            // 
            // shuffleEncounters
            // 
            this.shuffleEncounters.AutoSize = true;
            this.shuffleEncounters.Location = new System.Drawing.Point(6, 68);
            this.shuffleEncounters.Name = "shuffleEncounters";
            this.shuffleEncounters.Size = new System.Drawing.Size(116, 17);
            this.shuffleEncounters.TabIndex = 14;
            this.shuffleEncounters.Text = "Shuffle Encounters";
            this.toolTip1.SetToolTip(this.shuffleEncounters, "Shuffle which overworld encounters occur on different terrain types");
            this.shuffleEncounters.UseVisualStyleBackColor = true;
            this.shuffleEncounters.CheckedChanged += new System.EventHandler(this.shuffleEncounters_CheckedChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label43);
            this.tabPage2.Controls.Add(this.palaceBox);
            this.tabPage2.Controls.Add(this.bossRoomBox);
            this.tabPage2.Controls.Add(this.blockerBox);
            this.tabPage2.Controls.Add(this.customRooms);
            this.tabPage2.Controls.Add(this.bossItem);
            this.tabPage2.Controls.Add(this.removeTbird);
            this.tabPage2.Controls.Add(this.gpBox);
            this.tabPage2.Controls.Add(this.upaBox);
            this.tabPage2.Controls.Add(this.palacePalette);
            this.tabPage2.Controls.Add(this.tbirdBox);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.numGemsCbo);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(509, 285);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Palaces";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // bossItem
            // 
            this.bossItem.AutoSize = true;
            this.bossItem.Location = new System.Drawing.Point(241, 52);
            this.bossItem.Name = "bossItem";
            this.bossItem.Size = new System.Drawing.Size(154, 17);
            this.bossItem.TabIndex = 8;
            this.bossItem.Text = "Randomize Boss Item Drop";
            this.toolTip1.SetToolTip(this.bossItem, "When selected, the item that drops after a boss has been killed will be randomize" +
        "d");
            this.bossItem.UseVisualStyleBackColor = true;
            // 
            // removeTbird
            // 
            this.removeTbird.AutoSize = true;
            this.removeTbird.Location = new System.Drawing.Point(6, 185);
            this.removeTbird.Name = "removeTbird";
            this.removeTbird.Size = new System.Drawing.Size(126, 17);
            this.removeTbird.TabIndex = 7;
            this.removeTbird.Text = "Remove Thunderbird";
            this.toolTip1.SetToolTip(this.removeTbird, "If checked, you must defeat thunderbird");
            this.removeTbird.UseVisualStyleBackColor = true;
            this.removeTbird.CheckedChanged += new System.EventHandler(this.removeTbird_CheckedChanged);
            // 
            // gpBox
            // 
            this.gpBox.AutoSize = true;
            this.gpBox.Location = new System.Drawing.Point(6, 139);
            this.gpBox.Name = "gpBox";
            this.gpBox.Size = new System.Drawing.Size(128, 17);
            this.gpBox.TabIndex = 6;
            this.gpBox.Text = "Shorten Great Palace";
            this.toolTip1.SetToolTip(this.gpBox, "When selected, the Great Palace will have fewer rooms than normal");
            this.gpBox.UseVisualStyleBackColor = true;
            // 
            // upaBox
            // 
            this.upaBox.AutoSize = true;
            this.upaBox.Location = new System.Drawing.Point(241, 6);
            this.upaBox.Name = "upaBox";
            this.upaBox.Size = new System.Drawing.Size(180, 17);
            this.upaBox.TabIndex = 5;
            this.upaBox.Text = "Restart at palaces on game over";
            this.toolTip1.SetToolTip(this.upaBox, "When selected, if you game over in a palace, you will restart at that palace inst" +
        "ead of the normal starting spot");
            this.upaBox.UseVisualStyleBackColor = true;
            // 
            // palacePalette
            // 
            this.palacePalette.AutoSize = true;
            this.palacePalette.Location = new System.Drawing.Point(241, 29);
            this.palacePalette.Name = "palacePalette";
            this.palacePalette.Size = new System.Drawing.Size(140, 17);
            this.palacePalette.TabIndex = 4;
            this.palacePalette.Text = "Change Palace Palettes";
            this.toolTip1.SetToolTip(this.palacePalette, "This option changes the colors and tileset of palaces");
            this.palacePalette.UseVisualStyleBackColor = true;
            // 
            // tbirdBox
            // 
            this.tbirdBox.AutoSize = true;
            this.tbirdBox.Location = new System.Drawing.Point(6, 162);
            this.tbirdBox.Name = "tbirdBox";
            this.tbirdBox.Size = new System.Drawing.Size(129, 17);
            this.tbirdBox.TabIndex = 3;
            this.tbirdBox.Text = "Thunderbird Required";
            this.toolTip1.SetToolTip(this.tbirdBox, "If checked, you must defeat thunderbird");
            this.tbirdBox.UseVisualStyleBackColor = true;
            this.tbirdBox.CheckedChanged += new System.EventHandler(this.tbirdBox_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 243);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Number of Palaces to Complete";
            // 
            // numGemsCbo
            // 
            this.numGemsCbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.numGemsCbo.FormattingEnabled = true;
            this.numGemsCbo.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "Random"});
            this.numGemsCbo.Location = new System.Drawing.Point(6, 259);
            this.numGemsCbo.Name = "numGemsCbo";
            this.numGemsCbo.Size = new System.Drawing.Size(151, 21);
            this.numGemsCbo.TabIndex = 1;
            this.toolTip1.SetToolTip(this.numGemsCbo, "How many gems need to be placed before entering palace 7");
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.lifeEffBox);
            this.tabPage5.Controls.Add(this.magEffBox);
            this.tabPage5.Controls.Add(this.atkEffBox);
            this.tabPage5.Controls.Add(this.scaleLevels);
            this.tabPage5.Controls.Add(this.label30);
            this.tabPage5.Controls.Add(this.lifeCapBox);
            this.tabPage5.Controls.Add(this.magCapBox);
            this.tabPage5.Controls.Add(this.atkCapBox);
            this.tabPage5.Controls.Add(this.label29);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.label27);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Controls.Add(this.label11);
            this.tabPage5.Controls.Add(this.label10);
            this.tabPage5.Controls.Add(this.expBox);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(509, 285);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Levels";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // lifeEffBox
            // 
            this.lifeEffBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lifeEffBox.FormattingEnabled = true;
            this.lifeEffBox.Items.AddRange(new object[] {
            "Random",
            "OHKO Link",
            "Vanilla",
            "High Defense",
            "Invincible"});
            this.lifeEffBox.Location = new System.Drawing.Point(336, 96);
            this.lifeEffBox.Name = "lifeEffBox";
            this.lifeEffBox.Size = new System.Drawing.Size(121, 21);
            this.lifeEffBox.TabIndex = 25;
            this.toolTip1.SetToolTip(this.lifeEffBox, "Different modes for the effectiveness of Life levels");
            // 
            // magEffBox
            // 
            this.magEffBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.magEffBox.FormattingEnabled = true;
            this.magEffBox.Items.AddRange(new object[] {
            "Random",
            "High Spell Cost",
            "Vanilla",
            "Low Spell Cost",
            "Free Spells"});
            this.magEffBox.Location = new System.Drawing.Point(336, 57);
            this.magEffBox.Name = "magEffBox";
            this.magEffBox.Size = new System.Drawing.Size(121, 21);
            this.magEffBox.TabIndex = 24;
            this.toolTip1.SetToolTip(this.magEffBox, "Different modes for the effectiveness of Magic levels");
            // 
            // atkEffBox
            // 
            this.atkEffBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.atkEffBox.FormattingEnabled = true;
            this.atkEffBox.Items.AddRange(new object[] {
            "Random",
            "Low Attack",
            "Vanilla",
            "High Attack",
            "OHKO Enemies"});
            this.atkEffBox.Location = new System.Drawing.Point(336, 20);
            this.atkEffBox.Name = "atkEffBox";
            this.atkEffBox.Size = new System.Drawing.Size(121, 21);
            this.atkEffBox.TabIndex = 23;
            this.toolTip1.SetToolTip(this.atkEffBox, "Different modes for the effectiveness of Attack levels");
            // 
            // scaleLevels
            // 
            this.scaleLevels.AutoSize = true;
            this.scaleLevels.Location = new System.Drawing.Point(9, 163);
            this.scaleLevels.Name = "scaleLevels";
            this.scaleLevels.Size = new System.Drawing.Size(184, 17);
            this.scaleLevels.TabIndex = 22;
            this.scaleLevels.Text = "Scale Level Requirements to Cap";
            this.toolTip1.SetToolTip(this.scaleLevels, "When selected, experience requirements will be scaled up based on the maximum lev" +
        "el from the level cap");
            this.scaleLevels.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(75, 94);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(55, 13);
            this.label30.TabIndex = 21;
            this.label30.Text = "Level Cap";
            // 
            // lifeCapBox
            // 
            this.lifeCapBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lifeCapBox.FormattingEnabled = true;
            this.lifeCapBox.Items.AddRange(new object[] {
            "8",
            "7",
            "6",
            "5",
            "4",
            "3",
            "2",
            "1"});
            this.lifeCapBox.Location = new System.Drawing.Point(123, 113);
            this.lifeCapBox.Name = "lifeCapBox";
            this.lifeCapBox.Size = new System.Drawing.Size(32, 21);
            this.lifeCapBox.TabIndex = 20;
            this.toolTip1.SetToolTip(this.lifeCapBox, "Maximum Life Level");
            this.lifeCapBox.SelectedIndexChanged += new System.EventHandler(this.enableLevelScaling);
            // 
            // magCapBox
            // 
            this.magCapBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.magCapBox.FormattingEnabled = true;
            this.magCapBox.Items.AddRange(new object[] {
            "8",
            "7",
            "6",
            "5",
            "4",
            "3",
            "2",
            "1"});
            this.magCapBox.Location = new System.Drawing.Point(85, 113);
            this.magCapBox.Name = "magCapBox";
            this.magCapBox.Size = new System.Drawing.Size(32, 21);
            this.magCapBox.TabIndex = 19;
            this.toolTip1.SetToolTip(this.magCapBox, "Maximum Magic Level");
            this.magCapBox.SelectedIndexChanged += new System.EventHandler(this.enableLevelScaling);
            // 
            // atkCapBox
            // 
            this.atkCapBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.atkCapBox.FormattingEnabled = true;
            this.atkCapBox.Items.AddRange(new object[] {
            "8",
            "7",
            "6",
            "5",
            "4",
            "3",
            "2",
            "1"});
            this.atkCapBox.Location = new System.Drawing.Point(47, 113);
            this.atkCapBox.Name = "atkCapBox";
            this.atkCapBox.Size = new System.Drawing.Size(32, 21);
            this.atkCapBox.TabIndex = 18;
            this.toolTip1.SetToolTip(this.atkCapBox, "Maximum Attack Level");
            this.atkCapBox.SelectedIndexChanged += new System.EventHandler(this.enableLevelScaling);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(127, 141);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(24, 13);
            this.label29.TabIndex = 17;
            this.label29.Text = "Life";
            this.toolTip1.SetToolTip(this.label29, "Maximum Life Level");
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(87, 141);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(28, 13);
            this.label28.TabIndex = 16;
            this.label28.Text = "Mag";
            this.toolTip1.SetToolTip(this.label28, "Maximum Magic Level");
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(52, 141);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(23, 13);
            this.label27.TabIndex = 15;
            this.label27.Text = "Atk";
            this.toolTip1.SetToolTip(this.label27, "Maximum Attack Level");
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(224, 99);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Life Effectiveness:";
            this.toolTip1.SetToolTip(this.label12, "Different modes for the effectiveness of Life levels");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(224, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Magic Effectiveness:";
            this.toolTip1.SetToolTip(this.label11, "Different modes for the effectiveness of Magic levels");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(224, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Attack Effectiveness:";
            this.toolTip1.SetToolTip(this.label10, "Different modes for the effectiveness of Attack levels");
            // 
            // expBox
            // 
            this.expBox.Controls.Add(this.lifeExpNeeded);
            this.expBox.Controls.Add(this.magicExpNeeded);
            this.expBox.Controls.Add(this.shuffleAtkExp);
            this.expBox.Controls.Add(this.shuffleAllExp);
            this.expBox.Location = new System.Drawing.Point(3, 3);
            this.expBox.Name = "expBox";
            this.expBox.Size = new System.Drawing.Size(211, 86);
            this.expBox.TabIndex = 0;
            this.expBox.TabStop = false;
            this.expBox.Text = "                                                      ";
            // 
            // lifeExpNeeded
            // 
            this.lifeExpNeeded.AutoSize = true;
            this.lifeExpNeeded.Location = new System.Drawing.Point(18, 65);
            this.lifeExpNeeded.Name = "lifeExpNeeded";
            this.lifeExpNeeded.Size = new System.Drawing.Size(176, 17);
            this.lifeExpNeeded.TabIndex = 3;
            this.lifeExpNeeded.Text = "Shuffle Life Experience Needed";
            this.toolTip1.SetToolTip(this.lifeExpNeeded, "Shuffles experience needed for life levels");
            this.lifeExpNeeded.UseVisualStyleBackColor = true;
            // 
            // magicExpNeeded
            // 
            this.magicExpNeeded.AutoSize = true;
            this.magicExpNeeded.Location = new System.Drawing.Point(18, 42);
            this.magicExpNeeded.Name = "magicExpNeeded";
            this.magicExpNeeded.Size = new System.Drawing.Size(188, 17);
            this.magicExpNeeded.TabIndex = 2;
            this.magicExpNeeded.Text = "Shuffle Magic Experience Needed";
            this.toolTip1.SetToolTip(this.magicExpNeeded, "Shuffles experience needed for magic levels");
            this.magicExpNeeded.UseVisualStyleBackColor = true;
            // 
            // shuffleAtkExp
            // 
            this.shuffleAtkExp.AutoSize = true;
            this.shuffleAtkExp.Location = new System.Drawing.Point(18, 19);
            this.shuffleAtkExp.Name = "shuffleAtkExp";
            this.shuffleAtkExp.Size = new System.Drawing.Size(190, 17);
            this.shuffleAtkExp.TabIndex = 1;
            this.shuffleAtkExp.Text = "Shuffle Attack Experience Needed";
            this.toolTip1.SetToolTip(this.shuffleAtkExp, "Shuffles experience needed for attack levels");
            this.shuffleAtkExp.UseVisualStyleBackColor = true;
            // 
            // shuffleAllExp
            // 
            this.shuffleAllExp.AutoSize = true;
            this.shuffleAllExp.Location = new System.Drawing.Point(6, 0);
            this.shuffleAllExp.Name = "shuffleAllExp";
            this.shuffleAllExp.Size = new System.Drawing.Size(170, 17);
            this.shuffleAllExp.TabIndex = 1;
            this.shuffleAllExp.Text = "Shuffle All Experience Needed";
            this.toolTip1.SetToolTip(this.shuffleAllExp, "Shuffles experience needed for all levels");
            this.shuffleAllExp.UseVisualStyleBackColor = true;
            this.shuffleAllExp.CheckedChanged += new System.EventHandler(this.shuffleAllExp_CheckedChanged);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.dashBox);
            this.tabPage9.Controls.Add(this.spellEnemy);
            this.tabPage9.Controls.Add(this.combineFireBox);
            this.tabPage9.Controls.Add(this.disableJarBox);
            this.tabPage9.Controls.Add(this.shuffleSpellLocationsBox);
            this.tabPage9.Controls.Add(this.lifeRefilBox);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(509, 285);
            this.tabPage9.TabIndex = 9;
            this.tabPage9.Text = "Spells";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // spellEnemy
            // 
            this.spellEnemy.AutoSize = true;
            this.spellEnemy.Location = new System.Drawing.Point(6, 99);
            this.spellEnemy.Name = "spellEnemy";
            this.spellEnemy.Size = new System.Drawing.Size(166, 17);
            this.spellEnemy.TabIndex = 20;
            this.spellEnemy.Text = "Randomize Spell Spell Enemy";
            this.toolTip1.SetToolTip(this.spellEnemy, "When selected, the enemy generated when the Spell spell is cast will be randomize" +
        "d");
            this.spellEnemy.UseVisualStyleBackColor = true;
            // 
            // combineFireBox
            // 
            this.combineFireBox.AutoSize = true;
            this.combineFireBox.Location = new System.Drawing.Point(6, 76);
            this.combineFireBox.Name = "combineFireBox";
            this.combineFireBox.Size = new System.Drawing.Size(187, 17);
            this.combineFireBox.TabIndex = 19;
            this.combineFireBox.Text = "Combine Fire with a Random Spell";
            this.combineFireBox.UseVisualStyleBackColor = true;
            this.combineFireBox.CheckedChanged += new System.EventHandler(this.combineFireBox_CheckedChanged);
            // 
            // disableJarBox
            // 
            this.disableJarBox.AutoSize = true;
            this.disableJarBox.Location = new System.Drawing.Point(6, 53);
            this.disableJarBox.Name = "disableJarBox";
            this.disableJarBox.Size = new System.Drawing.Size(209, 17);
            this.disableJarBox.TabIndex = 18;
            this.disableJarBox.Text = "Disable Magic Container Requirements";
            this.toolTip1.SetToolTip(this.disableJarBox, "When checked, you can get spells without having the necessary magic containers");
            this.disableJarBox.UseVisualStyleBackColor = true;
            // 
            // shuffleSpellLocationsBox
            // 
            this.shuffleSpellLocationsBox.AutoSize = true;
            this.shuffleSpellLocationsBox.Location = new System.Drawing.Point(6, 30);
            this.shuffleSpellLocationsBox.Name = "shuffleSpellLocationsBox";
            this.shuffleSpellLocationsBox.Size = new System.Drawing.Size(134, 17);
            this.shuffleSpellLocationsBox.TabIndex = 17;
            this.shuffleSpellLocationsBox.Text = "Shuffle Spell Locations";
            this.toolTip1.SetToolTip(this.shuffleSpellLocationsBox, "This option shuffles which towns you find the spells in");
            this.shuffleSpellLocationsBox.UseVisualStyleBackColor = true;
            // 
            // lifeRefilBox
            // 
            this.lifeRefilBox.AutoSize = true;
            this.lifeRefilBox.Location = new System.Drawing.Point(6, 7);
            this.lifeRefilBox.Name = "lifeRefilBox";
            this.lifeRefilBox.Size = new System.Drawing.Size(144, 17);
            this.lifeRefilBox.TabIndex = 16;
            this.lifeRefilBox.Text = "Shuffle Life Refill Amount";
            this.toolTip1.SetToolTip(this.lifeRefilBox, "Shuffles how much health is restored when the life spell is used");
            this.lifeRefilBox.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.label31);
            this.tabPage6.Controls.Add(this.expDropBox);
            this.tabPage6.Controls.Add(this.shuffleDripper);
            this.tabPage6.Controls.Add(this.mixEnemies);
            this.tabPage6.Controls.Add(this.label8);
            this.tabPage6.Controls.Add(this.shufflePalaceEnemies);
            this.tabPage6.Controls.Add(this.shuffleOverworldEnemies);
            this.tabPage6.Controls.Add(this.swordImmuneBox);
            this.tabPage6.Controls.Add(this.stealExpAmt);
            this.tabPage6.Controls.Add(this.stealExpBox);
            this.tabPage6.Controls.Add(this.shuffleEnemyHPBox);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(509, 285);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Enemies";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(194, 127);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(129, 13);
            this.label31.TabIndex = 22;
            this.label31.Text = "Enemy Experience Drops:";
            this.toolTip1.SetToolTip(this.label31, "Different modes for how much experience the enemies drop");
            // 
            // expDropBox
            // 
            this.expDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.expDropBox.FormattingEnabled = true;
            this.expDropBox.Items.AddRange(new object[] {
            "Vanilla",
            "None",
            "Low",
            "Average",
            "High"});
            this.expDropBox.Location = new System.Drawing.Point(329, 124);
            this.expDropBox.Name = "expDropBox";
            this.expDropBox.Size = new System.Drawing.Size(121, 21);
            this.expDropBox.TabIndex = 21;
            this.toolTip1.SetToolTip(this.expDropBox, "Different modes for how much experience the enemies drop");
            // 
            // shuffleDripper
            // 
            this.shuffleDripper.AutoSize = true;
            this.shuffleDripper.Location = new System.Drawing.Point(3, 51);
            this.shuffleDripper.Name = "shuffleDripper";
            this.shuffleDripper.Size = new System.Drawing.Size(131, 17);
            this.shuffleDripper.TabIndex = 20;
            this.shuffleDripper.Text = "Shuffle Dripper Enemy";
            this.toolTip1.SetToolTip(this.shuffleDripper, "When selected, the enemy spawned by the dripper will be randomized");
            this.shuffleDripper.UseVisualStyleBackColor = true;
            // 
            // mixEnemies
            // 
            this.mixEnemies.AutoSize = true;
            this.mixEnemies.Location = new System.Drawing.Point(3, 74);
            this.mixEnemies.Name = "mixEnemies";
            this.mixEnemies.Size = new System.Drawing.Size(164, 17);
            this.mixEnemies.TabIndex = 18;
            this.mixEnemies.Text = "Mix Large and Small Enemies";
            this.toolTip1.SetToolTip(this.mixEnemies, "Allows large enemies to spawn where small enemies normally spawn and vice versa");
            this.mixEnemies.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(173, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(2, 195);
            this.label8.TabIndex = 17;
            // 
            // shufflePalaceEnemies
            // 
            this.shufflePalaceEnemies.AutoSize = true;
            this.shufflePalaceEnemies.Location = new System.Drawing.Point(3, 28);
            this.shufflePalaceEnemies.Name = "shufflePalaceEnemies";
            this.shufflePalaceEnemies.Size = new System.Drawing.Size(138, 17);
            this.shufflePalaceEnemies.TabIndex = 8;
            this.shufflePalaceEnemies.Text = "Shuffle Palace Enemies";
            this.toolTip1.SetToolTip(this.shufflePalaceEnemies, "Shuffles enemies in the palaces");
            this.shufflePalaceEnemies.UseVisualStyleBackColor = true;
            this.shufflePalaceEnemies.CheckedChanged += new System.EventHandler(this.shufflePalaceEnemies_CheckedChanged);
            // 
            // shuffleOverworldEnemies
            // 
            this.shuffleOverworldEnemies.AutoSize = true;
            this.shuffleOverworldEnemies.Location = new System.Drawing.Point(3, 5);
            this.shuffleOverworldEnemies.Name = "shuffleOverworldEnemies";
            this.shuffleOverworldEnemies.Size = new System.Drawing.Size(153, 17);
            this.shuffleOverworldEnemies.TabIndex = 7;
            this.shuffleOverworldEnemies.Text = "Shuffle Overworld Enemies";
            this.toolTip1.SetToolTip(this.shuffleOverworldEnemies, "Shuffles enemies on the overworld");
            this.shuffleOverworldEnemies.UseVisualStyleBackColor = true;
            this.shuffleOverworldEnemies.CheckedChanged += new System.EventHandler(this.shuffleOverworldEnemies_CheckedChanged);
            // 
            // swordImmuneBox
            // 
            this.swordImmuneBox.AutoSize = true;
            this.swordImmuneBox.Location = new System.Drawing.Point(194, 74);
            this.swordImmuneBox.Name = "swordImmuneBox";
            this.swordImmuneBox.Size = new System.Drawing.Size(136, 17);
            this.swordImmuneBox.TabIndex = 4;
            this.swordImmuneBox.Text = "Shuffle Sword Immunity";
            this.toolTip1.SetToolTip(this.swordImmuneBox, "Shuffle which enemies require fire to kill");
            this.swordImmuneBox.UseVisualStyleBackColor = true;
            // 
            // stealExpAmt
            // 
            this.stealExpAmt.AutoSize = true;
            this.stealExpAmt.Location = new System.Drawing.Point(194, 51);
            this.stealExpAmt.Name = "stealExpAmt";
            this.stealExpAmt.Size = new System.Drawing.Size(164, 17);
            this.stealExpAmt.TabIndex = 3;
            this.stealExpAmt.Text = "Shuffle Amount of Exp Stolen";
            this.toolTip1.SetToolTip(this.stealExpAmt, "Shuffle how much experience is stolen from the player when taking damage from cer" +
        "tain enemies");
            this.stealExpAmt.UseVisualStyleBackColor = true;
            // 
            // stealExpBox
            // 
            this.stealExpBox.AutoSize = true;
            this.stealExpBox.Location = new System.Drawing.Point(194, 28);
            this.stealExpBox.Name = "stealExpBox";
            this.stealExpBox.Size = new System.Drawing.Size(184, 17);
            this.stealExpBox.TabIndex = 2;
            this.stealExpBox.Text = "Shuffle Which Enemies Steal Exp";
            this.toolTip1.SetToolTip(this.stealExpBox, "Shuffle which enemies steal experience when doing damage to the player");
            this.stealExpBox.UseVisualStyleBackColor = true;
            // 
            // shuffleEnemyHPBox
            // 
            this.shuffleEnemyHPBox.AutoSize = true;
            this.shuffleEnemyHPBox.Location = new System.Drawing.Point(194, 5);
            this.shuffleEnemyHPBox.Name = "shuffleEnemyHPBox";
            this.shuffleEnemyHPBox.Size = new System.Drawing.Size(112, 17);
            this.shuffleEnemyHPBox.TabIndex = 0;
            this.shuffleEnemyHPBox.Text = "Shuffle Enemy HP";
            this.toolTip1.SetToolTip(this.shuffleEnemyHPBox, "Each enemy will have +/- 50% of its normal HP");
            this.shuffleEnemyHPBox.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.shufflePbagExp);
            this.tabPage7.Controls.Add(this.spellItemBox);
            this.tabPage7.Controls.Add(this.pbagItemShuffleBox);
            this.tabPage7.Controls.Add(this.kasutoBox);
            this.tabPage7.Controls.Add(this.palaceKeys);
            this.tabPage7.Controls.Add(this.shuffleSmallItemsBox);
            this.tabPage7.Controls.Add(this.mixItemBox);
            this.tabPage7.Controls.Add(this.overworldItemBox);
            this.tabPage7.Controls.Add(this.palaceItemBox);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(509, 285);
            this.tabPage7.TabIndex = 7;
            this.tabPage7.Text = "Items";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // shufflePbagExp
            // 
            this.shufflePbagExp.AutoSize = true;
            this.shufflePbagExp.Location = new System.Drawing.Point(3, 186);
            this.shufflePbagExp.Margin = new System.Windows.Forms.Padding(2);
            this.shufflePbagExp.Name = "shufflePbagExp";
            this.shufflePbagExp.Size = new System.Drawing.Size(131, 17);
            this.shufflePbagExp.TabIndex = 19;
            this.shufflePbagExp.Text = "Shuffle Pbag Amounts";
            this.toolTip1.SetToolTip(this.shufflePbagExp, "If selected, the pbag amounts will be randomized.");
            this.shufflePbagExp.UseVisualStyleBackColor = true;
            // 
            // spellItemBox
            // 
            this.spellItemBox.AutoSize = true;
            this.spellItemBox.Location = new System.Drawing.Point(3, 163);
            this.spellItemBox.Margin = new System.Windows.Forms.Padding(2);
            this.spellItemBox.Name = "spellItemBox";
            this.spellItemBox.Size = new System.Drawing.Size(120, 17);
            this.spellItemBox.TabIndex = 18;
            this.spellItemBox.Text = "Remove Spell Items";
            this.toolTip1.SetToolTip(this.spellItemBox, "When checked, you no longer need the trophy, medicine, or kid to access the respe" +
        "ctive spells");
            this.spellItemBox.UseVisualStyleBackColor = true;
            this.spellItemBox.CheckedChanged += new System.EventHandler(this.spellItemBox_CheckedChanged);
            // 
            // pbagItemShuffleBox
            // 
            this.pbagItemShuffleBox.AutoSize = true;
            this.pbagItemShuffleBox.Location = new System.Drawing.Point(3, 72);
            this.pbagItemShuffleBox.Name = "pbagItemShuffleBox";
            this.pbagItemShuffleBox.Size = new System.Drawing.Size(192, 17);
            this.pbagItemShuffleBox.TabIndex = 17;
            this.pbagItemShuffleBox.Text = "Include Pbag Caves in Item Shuffle";
            this.toolTip1.SetToolTip(this.pbagItemShuffleBox, "Will include the 3 pbag caves as item locations");
            this.pbagItemShuffleBox.UseVisualStyleBackColor = true;
            // 
            // kasutoBox
            // 
            this.kasutoBox.AutoSize = true;
            this.kasutoBox.Location = new System.Drawing.Point(3, 141);
            this.kasutoBox.Name = "kasutoBox";
            this.kasutoBox.Size = new System.Drawing.Size(225, 17);
            this.kasutoBox.TabIndex = 16;
            this.kasutoBox.Text = "Randomize New Kasuto Jar Requirements";
            this.toolTip1.SetToolTip(this.kasutoBox, "When selected, the number of jars required to get the item in New Kasuto will be " +
        "randomized between 5 and 7.");
            this.kasutoBox.UseVisualStyleBackColor = true;
            // 
            // palaceKeys
            // 
            this.palaceKeys.AutoSize = true;
            this.palaceKeys.Location = new System.Drawing.Point(3, 118);
            this.palaceKeys.Name = "palaceKeys";
            this.palaceKeys.Size = new System.Drawing.Size(156, 17);
            this.palaceKeys.TabIndex = 4;
            this.palaceKeys.Text = "Palaces Contain Extra Keys";
            this.toolTip1.SetToolTip(this.palaceKeys, "Inserts a lot of extra keys into the palaces");
            this.palaceKeys.UseVisualStyleBackColor = true;
            // 
            // shuffleSmallItemsBox
            // 
            this.shuffleSmallItemsBox.AutoSize = true;
            this.shuffleSmallItemsBox.Location = new System.Drawing.Point(3, 95);
            this.shuffleSmallItemsBox.Name = "shuffleSmallItemsBox";
            this.shuffleSmallItemsBox.Size = new System.Drawing.Size(115, 17);
            this.shuffleSmallItemsBox.TabIndex = 3;
            this.shuffleSmallItemsBox.Text = "Shuffle Small Items";
            this.toolTip1.SetToolTip(this.shuffleSmallItemsBox, "Shuffles pbags, jars, and 1ups");
            this.shuffleSmallItemsBox.UseVisualStyleBackColor = true;
            // 
            // mixItemBox
            // 
            this.mixItemBox.AutoSize = true;
            this.mixItemBox.Location = new System.Drawing.Point(3, 49);
            this.mixItemBox.Name = "mixItemBox";
            this.mixItemBox.Size = new System.Drawing.Size(178, 17);
            this.mixItemBox.TabIndex = 2;
            this.mixItemBox.Text = "Mix Overworld and Palace Items";
            this.toolTip1.SetToolTip(this.mixItemBox, "Allows palace items to be found in the overworld, and vice versa");
            this.mixItemBox.UseVisualStyleBackColor = true;
            // 
            // overworldItemBox
            // 
            this.overworldItemBox.AutoSize = true;
            this.overworldItemBox.Location = new System.Drawing.Point(3, 26);
            this.overworldItemBox.Name = "overworldItemBox";
            this.overworldItemBox.Size = new System.Drawing.Size(138, 17);
            this.overworldItemBox.TabIndex = 1;
            this.overworldItemBox.Text = "Shuffle Overworld Items";
            this.toolTip1.SetToolTip(this.overworldItemBox, "Shuffles the items that are found in the overworld");
            this.overworldItemBox.UseVisualStyleBackColor = true;
            this.overworldItemBox.CheckedChanged += new System.EventHandler(this.overworldItemBox_CheckedChanged);
            // 
            // palaceItemBox
            // 
            this.palaceItemBox.AutoSize = true;
            this.palaceItemBox.Location = new System.Drawing.Point(3, 3);
            this.palaceItemBox.Name = "palaceItemBox";
            this.palaceItemBox.Size = new System.Drawing.Size(123, 17);
            this.palaceItemBox.TabIndex = 0;
            this.palaceItemBox.Text = "Shuffle Palace Items";
            this.toolTip1.SetToolTip(this.palaceItemBox, "Shuffles the items that are found in palaces");
            this.palaceItemBox.UseVisualStyleBackColor = true;
            this.palaceItemBox.CheckedChanged += new System.EventHandler(this.palaceItemBox_CheckedChanged);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.randoDrops);
            this.tabPage8.Controls.Add(this.standardDrops);
            this.tabPage8.Controls.Add(this.largeKey);
            this.tabPage8.Controls.Add(this.large1up);
            this.tabPage8.Controls.Add(this.large500);
            this.tabPage8.Controls.Add(this.large200);
            this.tabPage8.Controls.Add(this.large100);
            this.tabPage8.Controls.Add(this.large50);
            this.tabPage8.Controls.Add(this.largeRedJar);
            this.tabPage8.Controls.Add(this.largeBlueJar);
            this.tabPage8.Controls.Add(this.label21);
            this.tabPage8.Controls.Add(this.smallKey);
            this.tabPage8.Controls.Add(this.small1up);
            this.tabPage8.Controls.Add(this.small500);
            this.tabPage8.Controls.Add(this.small200);
            this.tabPage8.Controls.Add(this.small100);
            this.tabPage8.Controls.Add(this.small50);
            this.tabPage8.Controls.Add(this.smallRedJar);
            this.tabPage8.Controls.Add(this.smallBlueJar);
            this.tabPage8.Controls.Add(this.label20);
            this.tabPage8.Controls.Add(this.label19);
            this.tabPage8.Controls.Add(this.enemyDropBox);
            this.tabPage8.Controls.Add(this.pbagDrop);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage8.Size = new System.Drawing.Size(509, 285);
            this.tabPage8.TabIndex = 8;
            this.tabPage8.Text = "Drops";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // randoDrops
            // 
            this.randoDrops.AutoSize = true;
            this.randoDrops.Location = new System.Drawing.Point(8, 52);
            this.randoDrops.Margin = new System.Windows.Forms.Padding(2);
            this.randoDrops.Name = "randoDrops";
            this.randoDrops.Size = new System.Drawing.Size(110, 17);
            this.randoDrops.TabIndex = 45;
            this.randoDrops.Text = "Randomize Drops";
            this.toolTip1.SetToolTip(this.randoDrops, "When selected, the items in the drop pool will be randomized");
            this.randoDrops.UseVisualStyleBackColor = true;
            this.randoDrops.CheckedChanged += new System.EventHandler(this.randoDrops_CheckedChanged);
            // 
            // standardDrops
            // 
            this.standardDrops.AutoSize = true;
            this.standardDrops.Location = new System.Drawing.Point(8, 73);
            this.standardDrops.Margin = new System.Windows.Forms.Padding(2);
            this.standardDrops.Name = "standardDrops";
            this.standardDrops.Size = new System.Drawing.Size(113, 17);
            this.standardDrops.TabIndex = 44;
            this.standardDrops.Text = "Standardize Drops";
            this.toolTip1.SetToolTip(this.standardDrops, "When selected, all runners playing the same seed will get the same drops in the s" +
        "ame order");
            this.standardDrops.UseVisualStyleBackColor = true;
            // 
            // largeKey
            // 
            this.largeKey.AutoSize = true;
            this.largeKey.Enabled = false;
            this.largeKey.Location = new System.Drawing.Point(346, 177);
            this.largeKey.Margin = new System.Windows.Forms.Padding(2);
            this.largeKey.Name = "largeKey";
            this.largeKey.Size = new System.Drawing.Size(44, 17);
            this.largeKey.TabIndex = 43;
            this.largeKey.Text = "Key";
            this.toolTip1.SetToolTip(this.largeKey, "Add small keys to the large enemy pool");
            this.largeKey.UseVisualStyleBackColor = true;
            // 
            // large1up
            // 
            this.large1up.AutoSize = true;
            this.large1up.Enabled = false;
            this.large1up.Location = new System.Drawing.Point(346, 156);
            this.large1up.Margin = new System.Windows.Forms.Padding(2);
            this.large1up.Name = "large1up";
            this.large1up.Size = new System.Drawing.Size(44, 17);
            this.large1up.TabIndex = 42;
            this.large1up.Text = "1up";
            this.toolTip1.SetToolTip(this.large1up, "Add 1ups to the large enemy pool");
            this.large1up.UseVisualStyleBackColor = true;
            // 
            // large500
            // 
            this.large500.AutoSize = true;
            this.large500.Enabled = false;
            this.large500.Location = new System.Drawing.Point(346, 135);
            this.large500.Margin = new System.Windows.Forms.Padding(2);
            this.large500.Name = "large500";
            this.large500.Size = new System.Drawing.Size(71, 17);
            this.large500.TabIndex = 41;
            this.large500.Text = "500 pbag";
            this.toolTip1.SetToolTip(this.large500, "Add 500 bags to the large enemy pool");
            this.large500.UseVisualStyleBackColor = true;
            // 
            // large200
            // 
            this.large200.AutoSize = true;
            this.large200.Checked = true;
            this.large200.CheckState = System.Windows.Forms.CheckState.Checked;
            this.large200.Enabled = false;
            this.large200.Location = new System.Drawing.Point(346, 114);
            this.large200.Margin = new System.Windows.Forms.Padding(2);
            this.large200.Name = "large200";
            this.large200.Size = new System.Drawing.Size(71, 17);
            this.large200.TabIndex = 40;
            this.large200.Text = "200 pbag";
            this.toolTip1.SetToolTip(this.large200, "Add 200 bags to the large enemy pool");
            this.large200.UseVisualStyleBackColor = true;
            // 
            // large100
            // 
            this.large100.AutoSize = true;
            this.large100.Enabled = false;
            this.large100.Location = new System.Drawing.Point(346, 94);
            this.large100.Margin = new System.Windows.Forms.Padding(2);
            this.large100.Name = "large100";
            this.large100.Size = new System.Drawing.Size(71, 17);
            this.large100.TabIndex = 39;
            this.large100.Text = "100 pbag";
            this.toolTip1.SetToolTip(this.large100, "Add 100 bags to the large enemy pool");
            this.large100.UseVisualStyleBackColor = true;
            // 
            // large50
            // 
            this.large50.AutoSize = true;
            this.large50.Enabled = false;
            this.large50.Location = new System.Drawing.Point(346, 73);
            this.large50.Margin = new System.Windows.Forms.Padding(2);
            this.large50.Name = "large50";
            this.large50.Size = new System.Drawing.Size(65, 17);
            this.large50.TabIndex = 38;
            this.large50.Text = "50 pbag";
            this.toolTip1.SetToolTip(this.large50, "Add 50 bags to the large enemy pool");
            this.large50.UseVisualStyleBackColor = true;
            // 
            // largeRedJar
            // 
            this.largeRedJar.AutoSize = true;
            this.largeRedJar.Checked = true;
            this.largeRedJar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.largeRedJar.Enabled = false;
            this.largeRedJar.Location = new System.Drawing.Point(346, 52);
            this.largeRedJar.Margin = new System.Windows.Forms.Padding(2);
            this.largeRedJar.Name = "largeRedJar";
            this.largeRedJar.Size = new System.Drawing.Size(63, 17);
            this.largeRedJar.TabIndex = 37;
            this.largeRedJar.Text = "Red Jar";
            this.toolTip1.SetToolTip(this.largeRedJar, "Add red jars to the large enemy pool");
            this.largeRedJar.UseVisualStyleBackColor = true;
            // 
            // largeBlueJar
            // 
            this.largeBlueJar.AutoSize = true;
            this.largeBlueJar.Enabled = false;
            this.largeBlueJar.Location = new System.Drawing.Point(346, 31);
            this.largeBlueJar.Margin = new System.Windows.Forms.Padding(2);
            this.largeBlueJar.Name = "largeBlueJar";
            this.largeBlueJar.Size = new System.Drawing.Size(64, 17);
            this.largeBlueJar.TabIndex = 36;
            this.largeBlueJar.Text = "Blue Jar";
            this.toolTip1.SetToolTip(this.largeBlueJar, "Add blue jars to the large enemy pool");
            this.largeBlueJar.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(336, 10);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(93, 13);
            this.label21.TabIndex = 35;
            this.label21.Text = "Large Enemy Pool";
            // 
            // smallKey
            // 
            this.smallKey.AutoSize = true;
            this.smallKey.Enabled = false;
            this.smallKey.Location = new System.Drawing.Point(218, 177);
            this.smallKey.Margin = new System.Windows.Forms.Padding(2);
            this.smallKey.Name = "smallKey";
            this.smallKey.Size = new System.Drawing.Size(44, 17);
            this.smallKey.TabIndex = 34;
            this.smallKey.Text = "Key";
            this.toolTip1.SetToolTip(this.smallKey, "Add small keys to the small enemy pool");
            this.smallKey.UseVisualStyleBackColor = true;
            // 
            // small1up
            // 
            this.small1up.AutoSize = true;
            this.small1up.Enabled = false;
            this.small1up.Location = new System.Drawing.Point(219, 156);
            this.small1up.Margin = new System.Windows.Forms.Padding(2);
            this.small1up.Name = "small1up";
            this.small1up.Size = new System.Drawing.Size(44, 17);
            this.small1up.TabIndex = 33;
            this.small1up.Text = "1up";
            this.toolTip1.SetToolTip(this.small1up, "Add 1ups to the small enemy pool");
            this.small1up.UseVisualStyleBackColor = true;
            // 
            // small500
            // 
            this.small500.AutoSize = true;
            this.small500.Enabled = false;
            this.small500.Location = new System.Drawing.Point(219, 135);
            this.small500.Margin = new System.Windows.Forms.Padding(2);
            this.small500.Name = "small500";
            this.small500.Size = new System.Drawing.Size(71, 17);
            this.small500.TabIndex = 32;
            this.small500.Text = "500 pbag";
            this.toolTip1.SetToolTip(this.small500, "Add 500 bags to the small enemy pool");
            this.small500.UseVisualStyleBackColor = true;
            // 
            // small200
            // 
            this.small200.AutoSize = true;
            this.small200.Enabled = false;
            this.small200.Location = new System.Drawing.Point(219, 114);
            this.small200.Margin = new System.Windows.Forms.Padding(2);
            this.small200.Name = "small200";
            this.small200.Size = new System.Drawing.Size(71, 17);
            this.small200.TabIndex = 31;
            this.small200.Text = "200 pbag";
            this.toolTip1.SetToolTip(this.small200, "Add 200 bags to the small enemy pool");
            this.small200.UseVisualStyleBackColor = true;
            // 
            // small100
            // 
            this.small100.AutoSize = true;
            this.small100.Enabled = false;
            this.small100.Location = new System.Drawing.Point(219, 94);
            this.small100.Margin = new System.Windows.Forms.Padding(2);
            this.small100.Name = "small100";
            this.small100.Size = new System.Drawing.Size(71, 17);
            this.small100.TabIndex = 30;
            this.small100.Text = "100 pbag";
            this.toolTip1.SetToolTip(this.small100, "Add 100 bags to the small enemy pool");
            this.small100.UseVisualStyleBackColor = true;
            // 
            // small50
            // 
            this.small50.AutoSize = true;
            this.small50.Checked = true;
            this.small50.CheckState = System.Windows.Forms.CheckState.Checked;
            this.small50.Enabled = false;
            this.small50.Location = new System.Drawing.Point(219, 73);
            this.small50.Margin = new System.Windows.Forms.Padding(2);
            this.small50.Name = "small50";
            this.small50.Size = new System.Drawing.Size(65, 17);
            this.small50.TabIndex = 29;
            this.small50.Text = "50 pbag";
            this.toolTip1.SetToolTip(this.small50, "Add 50 bags to the small enemy pool");
            this.small50.UseVisualStyleBackColor = true;
            // 
            // smallRedJar
            // 
            this.smallRedJar.AutoSize = true;
            this.smallRedJar.Enabled = false;
            this.smallRedJar.Location = new System.Drawing.Point(219, 52);
            this.smallRedJar.Margin = new System.Windows.Forms.Padding(2);
            this.smallRedJar.Name = "smallRedJar";
            this.smallRedJar.Size = new System.Drawing.Size(63, 17);
            this.smallRedJar.TabIndex = 28;
            this.smallRedJar.Text = "Red Jar";
            this.toolTip1.SetToolTip(this.smallRedJar, "Add red jars to the small enemy pool");
            this.smallRedJar.UseVisualStyleBackColor = true;
            // 
            // smallBlueJar
            // 
            this.smallBlueJar.AutoSize = true;
            this.smallBlueJar.Checked = true;
            this.smallBlueJar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.smallBlueJar.Enabled = false;
            this.smallBlueJar.Location = new System.Drawing.Point(219, 31);
            this.smallBlueJar.Margin = new System.Windows.Forms.Padding(2);
            this.smallBlueJar.Name = "smallBlueJar";
            this.smallBlueJar.Size = new System.Drawing.Size(64, 17);
            this.smallBlueJar.TabIndex = 27;
            this.smallBlueJar.Text = "Blue Jar";
            this.toolTip1.SetToolTip(this.smallBlueJar, "Add blue jars to the small enemy pool");
            this.smallBlueJar.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(208, 10);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "Small Enemy Pool";
            // 
            // label19
            // 
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.Location = new System.Drawing.Point(179, 10);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(2, 195);
            this.label19.TabIndex = 24;
            // 
            // enemyDropBox
            // 
            this.enemyDropBox.AutoSize = true;
            this.enemyDropBox.Location = new System.Drawing.Point(8, 31);
            this.enemyDropBox.Margin = new System.Windows.Forms.Padding(2);
            this.enemyDropBox.Name = "enemyDropBox";
            this.enemyDropBox.Size = new System.Drawing.Size(132, 17);
            this.enemyDropBox.TabIndex = 23;
            this.enemyDropBox.Text = "Manually Select Drops";
            this.toolTip1.SetToolTip(this.enemyDropBox, "When checked, you can select what items get dropped by enemies");
            this.enemyDropBox.UseVisualStyleBackColor = true;
            this.enemyDropBox.CheckedChanged += new System.EventHandler(this.enemyDropBox_CheckedChanged);
            // 
            // pbagDrop
            // 
            this.pbagDrop.AutoSize = true;
            this.pbagDrop.Location = new System.Drawing.Point(8, 10);
            this.pbagDrop.Name = "pbagDrop";
            this.pbagDrop.Size = new System.Drawing.Size(161, 17);
            this.pbagDrop.TabIndex = 22;
            this.pbagDrop.Text = "Shuffle Item Drop Frequency";
            this.toolTip1.SetToolTip(this.pbagDrop, "This option will shuffle how often enemies drop pbags and jars");
            this.pbagDrop.UseVisualStyleBackColor = true;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.townSpellHints);
            this.tabPage10.Controls.Add(this.spellItemHints);
            this.tabPage10.Controls.Add(this.communityBox);
            this.tabPage10.Controls.Add(this.helpfulHints);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(509, 285);
            this.tabPage10.TabIndex = 10;
            this.tabPage10.Text = "Hints";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // townSpellHints
            // 
            this.townSpellHints.AutoSize = true;
            this.townSpellHints.Location = new System.Drawing.Point(3, 46);
            this.townSpellHints.Name = "townSpellHints";
            this.townSpellHints.Size = new System.Drawing.Size(147, 17);
            this.townSpellHints.TabIndex = 23;
            this.townSpellHints.Text = "Enable Town Name Hints";
            this.toolTip1.SetToolTip(this.townSpellHints, "Signs at the beginning of town will tell you what spell is contained in the town." +
        "");
            this.townSpellHints.UseVisualStyleBackColor = true;
            // 
            // spellItemHints
            // 
            this.spellItemHints.AutoSize = true;
            this.spellItemHints.Location = new System.Drawing.Point(3, 25);
            this.spellItemHints.Name = "spellItemHints";
            this.spellItemHints.Size = new System.Drawing.Size(135, 17);
            this.spellItemHints.TabIndex = 22;
            this.spellItemHints.Text = "Enable Spell Item Hints";
            this.toolTip1.SetToolTip(this.spellItemHints, "The people who require spell items will tell you where the item can be found.");
            this.spellItemHints.UseVisualStyleBackColor = true;
            // 
            // communityBox
            // 
            this.communityBox.AutoSize = true;
            this.communityBox.Location = new System.Drawing.Point(3, 68);
            this.communityBox.Margin = new System.Windows.Forms.Padding(2);
            this.communityBox.Name = "communityBox";
            this.communityBox.Size = new System.Drawing.Size(104, 17);
            this.communityBox.TabIndex = 21;
            this.communityBox.Text = "Community Hints";
            this.toolTip1.SetToolTip(this.communityBox, "When selected, will replace some text with hints submitted by the community");
            this.communityBox.UseVisualStyleBackColor = true;
            // 
            // helpfulHints
            // 
            this.helpfulHints.AutoSize = true;
            this.helpfulHints.Location = new System.Drawing.Point(3, 3);
            this.helpfulHints.Name = "helpfulHints";
            this.helpfulHints.Size = new System.Drawing.Size(122, 17);
            this.helpfulHints.TabIndex = 0;
            this.helpfulHints.Text = "Enable Helpful Hints";
            this.toolTip1.SetToolTip(this.helpfulHints, "Townspeople will give you helpful hints as to where items are located.");
            this.helpfulHints.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.flashingOff);
            this.tabPage3.Controls.Add(this.upAC1);
            this.tabPage3.Controls.Add(this.beamCmbo);
            this.tabPage3.Controls.Add(this.label26);
            this.tabPage3.Controls.Add(this.shieldColor);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.tunicColor);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.spriteCmbo);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.disableMusicBox);
            this.tabPage3.Controls.Add(this.enemyPalette);
            this.tabPage3.Controls.Add(this.beamBox);
            this.tabPage3.Controls.Add(this.fastSpellBox);
            this.tabPage3.Controls.Add(this.jumpNormalbox);
            this.tabPage3.Controls.Add(this.disableLowHealthBeep);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(509, 285);
            this.tabPage3.TabIndex = 6;
            this.tabPage3.Text = "Misc.";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // flashingOff
            // 
            this.flashingOff.AutoSize = true;
            this.flashingOff.Location = new System.Drawing.Point(3, 153);
            this.flashingOff.Name = "flashingOff";
            this.flashingOff.Size = new System.Drawing.Size(169, 17);
            this.flashingOff.TabIndex = 33;
            this.flashingOff.Text = "Remove Flashing Upon Death";
            this.toolTip1.SetToolTip(this.flashingOff, "When selected, the flashing animation after Link\'s death will be removed.");
            this.flashingOff.UseVisualStyleBackColor = true;
            // 
            // upAC1
            // 
            this.upAC1.AutoSize = true;
            this.upAC1.Location = new System.Drawing.Point(3, 130);
            this.upAC1.Name = "upAC1";
            this.upAC1.Size = new System.Drawing.Size(226, 17);
            this.upAC1.TabIndex = 32;
            this.upAC1.Text = "Remap Up+A to Up+Select on Controller 1";
            this.toolTip1.SetToolTip(this.upAC1, "When selected, Up+A on controller 2 will be remapped to Up+Select on Controller 1" +
        "");
            this.upAC1.UseVisualStyleBackColor = true;
            // 
            // beamCmbo
            // 
            this.beamCmbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.beamCmbo.FormattingEnabled = true;
            this.beamCmbo.Items.AddRange(new object[] {
            "Default",
            "Fire",
            "Bubble",
            "Rock",
            "Axe",
            "Hammer",
            "Wizzrobe Beam",
            "Random"});
            this.beamCmbo.Location = new System.Drawing.Point(255, 167);
            this.beamCmbo.Margin = new System.Windows.Forms.Padding(2);
            this.beamCmbo.Name = "beamCmbo";
            this.beamCmbo.Size = new System.Drawing.Size(122, 21);
            this.beamCmbo.TabIndex = 31;
            this.toolTip1.SetToolTip(this.beamCmbo, "Allows you to select what the beam sprite will be");
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(252, 147);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 13);
            this.label26.TabIndex = 30;
            this.label26.Text = "Beam Sprite:";
            // 
            // shieldColor
            // 
            this.shieldColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.shieldColor.FormattingEnabled = true;
            this.shieldColor.Items.AddRange(new object[] {
            "Default",
            "Green",
            "Dark Green",
            "Aqua",
            "Dark Blue",
            "Purple",
            "Pink",
            "Orange",
            "Red",
            "Turd",
            "Random"});
            this.shieldColor.Location = new System.Drawing.Point(255, 118);
            this.shieldColor.Margin = new System.Windows.Forms.Padding(2);
            this.shieldColor.Name = "shieldColor";
            this.shieldColor.Size = new System.Drawing.Size(122, 21);
            this.shieldColor.TabIndex = 29;
            this.toolTip1.SetToolTip(this.shieldColor, "Changes the tunic color for shield");
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(252, 97);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "Shield Tunic Color:";
            // 
            // tunicColor
            // 
            this.tunicColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tunicColor.FormattingEnabled = true;
            this.tunicColor.Items.AddRange(new object[] {
            "Default",
            "Green",
            "Dark Green",
            "Aqua",
            "Dark Blue",
            "Purple",
            "Pink",
            "Orange",
            "Red",
            "Turd",
            "Random"});
            this.tunicColor.Location = new System.Drawing.Point(255, 69);
            this.tunicColor.Margin = new System.Windows.Forms.Padding(2);
            this.tunicColor.Name = "tunicColor";
            this.tunicColor.Size = new System.Drawing.Size(122, 21);
            this.tunicColor.TabIndex = 27;
            this.toolTip1.SetToolTip(this.tunicColor, "Changes the normal tunic color");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(252, 47);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "Normal Tunic Color:";
            // 
            // spriteCmbo
            // 
            this.spriteCmbo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.spriteCmbo.FormattingEnabled = true;
            this.spriteCmbo.Items.AddRange(new object[] {
            "Link",
            "Zelda",
            "Iron Knuckle",
            "Error",
            "Samus",
            "Simon",
            "Stalfos",
            "Vase Lady",
            "Ruto",
            "Yoshi",
            "Dragonlord",
            "Miria",
            "Crystalis",
            "Taco",
            "Pyramid",
            "Lady Link",
            "Hoodie Link",
            "GliitchWiitch",
            "Random"});
            this.spriteCmbo.Location = new System.Drawing.Point(255, 21);
            this.spriteCmbo.Margin = new System.Windows.Forms.Padding(2);
            this.spriteCmbo.Name = "spriteCmbo";
            this.spriteCmbo.Size = new System.Drawing.Size(122, 21);
            this.spriteCmbo.TabIndex = 25;
            this.toolTip1.SetToolTip(this.spriteCmbo, "Changes the playable character sprite");
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(252, 3);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "Character Sprite:";
            // 
            // disableMusicBox
            // 
            this.disableMusicBox.AutoSize = true;
            this.disableMusicBox.Location = new System.Drawing.Point(3, 23);
            this.disableMusicBox.Margin = new System.Windows.Forms.Padding(2);
            this.disableMusicBox.Name = "disableMusicBox";
            this.disableMusicBox.Size = new System.Drawing.Size(92, 17);
            this.disableMusicBox.TabIndex = 23;
            this.disableMusicBox.Text = "Disable Music";
            this.toolTip1.SetToolTip(this.disableMusicBox, "Disables most in game music");
            this.disableMusicBox.UseVisualStyleBackColor = true;
            // 
            // enemyPalette
            // 
            this.enemyPalette.AutoSize = true;
            this.enemyPalette.Location = new System.Drawing.Point(3, 85);
            this.enemyPalette.Name = "enemyPalette";
            this.enemyPalette.Size = new System.Drawing.Size(130, 17);
            this.enemyPalette.TabIndex = 22;
            this.enemyPalette.Text = "Shuffle Sprite Palettes";
            this.toolTip1.SetToolTip(this.enemyPalette, "When selected, sprite colors will be shuffled");
            this.enemyPalette.UseVisualStyleBackColor = true;
            // 
            // beamBox
            // 
            this.beamBox.AutoSize = true;
            this.beamBox.Location = new System.Drawing.Point(3, 107);
            this.beamBox.Name = "beamBox";
            this.beamBox.Size = new System.Drawing.Size(140, 17);
            this.beamBox.TabIndex = 19;
            this.beamBox.Text = "Permanent Beam Sword";
            this.toolTip1.SetToolTip(this.beamBox, "Gives Link beam sword regardless of how much health he has");
            this.beamBox.UseVisualStyleBackColor = true;
            // 
            // fastSpellBox
            // 
            this.fastSpellBox.AutoSize = true;
            this.fastSpellBox.Location = new System.Drawing.Point(3, 64);
            this.fastSpellBox.Name = "fastSpellBox";
            this.fastSpellBox.Size = new System.Drawing.Size(110, 17);
            this.fastSpellBox.TabIndex = 4;
            this.fastSpellBox.Text = "Fast Spell Casting";
            this.toolTip1.SetToolTip(this.fastSpellBox, "When checked, you do not have to open the pause menu before casting the selected " +
        "spell");
            this.fastSpellBox.UseVisualStyleBackColor = true;
            // 
            // jumpNormalbox
            // 
            this.jumpNormalbox.AutoSize = true;
            this.jumpNormalbox.Location = new System.Drawing.Point(3, 43);
            this.jumpNormalbox.Name = "jumpNormalbox";
            this.jumpNormalbox.Size = new System.Drawing.Size(104, 17);
            this.jumpNormalbox.TabIndex = 3;
            this.jumpNormalbox.Text = "Jump Always On";
            this.toolTip1.SetToolTip(this.jumpNormalbox, "The player will jump very high, as if the jump spell is always active");
            this.jumpNormalbox.UseVisualStyleBackColor = true;
            // 
            // disableLowHealthBeep
            // 
            this.disableLowHealthBeep.AutoSize = true;
            this.disableLowHealthBeep.Location = new System.Drawing.Point(3, 3);
            this.disableLowHealthBeep.Name = "disableLowHealthBeep";
            this.disableLowHealthBeep.Size = new System.Drawing.Size(146, 17);
            this.disableLowHealthBeep.TabIndex = 0;
            this.disableLowHealthBeep.Text = "Disable Low Health Beep";
            this.toolTip1.SetToolTip(this.disableLowHealthBeep, "Disables the beeping that happens when the player is low on health");
            this.disableLowHealthBeep.UseVisualStyleBackColor = true;
            // 
            // fileTextBox
            // 
            this.fileTextBox.Location = new System.Drawing.Point(12, 26);
            this.fileTextBox.Name = "fileTextBox";
            this.fileTextBox.Size = new System.Drawing.Size(134, 20);
            this.fileTextBox.TabIndex = 1;
            this.toolTip1.SetToolTip(this.fileTextBox, "Select a USA version of the Zelda 2 ROM");
            // 
            // seedTextBox
            // 
            this.seedTextBox.Location = new System.Drawing.Point(12, 64);
            this.seedTextBox.Name = "seedTextBox";
            this.seedTextBox.Size = new System.Drawing.Size(134, 20);
            this.seedTextBox.TabIndex = 2;
            this.toolTip1.SetToolTip(this.seedTextBox, "This represents the random values that will be used. A different seed results in " +
        "a different shuffled ROM.");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ROM File";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Seed";
            // 
            // seedBtn
            // 
            this.seedBtn.Location = new System.Drawing.Point(152, 62);
            this.seedBtn.Name = "seedBtn";
            this.seedBtn.Size = new System.Drawing.Size(75, 23);
            this.seedBtn.TabIndex = 6;
            this.seedBtn.Text = "Create Seed";
            this.seedBtn.UseVisualStyleBackColor = true;
            this.seedBtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // fileBtn
            // 
            this.fileBtn.Location = new System.Drawing.Point(152, 25);
            this.fileBtn.Name = "fileBtn";
            this.fileBtn.Size = new System.Drawing.Size(75, 23);
            this.fileBtn.TabIndex = 7;
            this.fileBtn.Text = "Browse...";
            this.fileBtn.UseVisualStyleBackColor = true;
            this.fileBtn.Click += new System.EventHandler(this.fileBtn_Click);
            // 
            // generateBtn
            // 
            this.generateBtn.Location = new System.Drawing.Point(421, 26);
            this.generateBtn.Name = "generateBtn";
            this.generateBtn.Size = new System.Drawing.Size(108, 21);
            this.generateBtn.TabIndex = 8;
            this.generateBtn.Text = "Generate ROM";
            this.toolTip1.SetToolTip(this.generateBtn, "Create the ROM");
            this.generateBtn.UseVisualStyleBackColor = true;
            this.generateBtn.Click += new System.EventHandler(this.generateBtn_Click);
            // 
            // flagBox
            // 
            this.flagBox.Location = new System.Drawing.Point(233, 26);
            this.flagBox.Name = "flagBox";
            this.flagBox.Size = new System.Drawing.Size(182, 20);
            this.flagBox.TabIndex = 9;
            this.toolTip1.SetToolTip(this.flagBox, "These flags represent the selected options. They can be copy/pasted.");
            this.flagBox.TextChanged += new System.EventHandler(this.flagBox_TextChanged);
            // 
            // updateBtn
            // 
            this.updateBtn.Location = new System.Drawing.Point(421, 61);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(108, 23);
            this.updateBtn.TabIndex = 10;
            this.updateBtn.Text = "Check for Updates";
            this.toolTip1.SetToolTip(this.updateBtn, "Check for updates");
            this.updateBtn.UseVisualStyleBackColor = true;
            this.updateBtn.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(234, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Flags";
            // 
            // wikiBtn
            // 
            this.wikiBtn.Location = new System.Drawing.Point(330, 61);
            this.wikiBtn.Name = "wikiBtn";
            this.wikiBtn.Size = new System.Drawing.Size(85, 23);
            this.wikiBtn.TabIndex = 12;
            this.wikiBtn.Text = "Wiki";
            this.toolTip1.SetToolTip(this.wikiBtn, "Visit the website");
            this.wikiBtn.UseVisualStyleBackColor = true;
            this.wikiBtn.Click += new System.EventHandler(this.wikiBtn_Click);
            // 
            // botBtn
            // 
            this.botBtn.Location = new System.Drawing.Point(29, 407);
            this.botBtn.Name = "botBtn";
            this.botBtn.Size = new System.Drawing.Size(75, 23);
            this.botBtn.TabIndex = 18;
            this.botBtn.Text = "Beginner";
            this.toolTip1.SetToolTip(this.botBtn, "This preset is great for people who are looking for a casual experience.");
            this.botBtn.UseVisualStyleBackColor = true;
            this.botBtn.Click += new System.EventHandler(this.beginnerFlags);
            // 
            // megmetBtn
            // 
            this.megmetBtn.Location = new System.Drawing.Point(110, 407);
            this.megmetBtn.Name = "megmetBtn";
            this.megmetBtn.Size = new System.Drawing.Size(75, 23);
            this.megmetBtn.TabIndex = 19;
            this.megmetBtn.Text = "Swiss";
            this.toolTip1.SetToolTip(this.megmetBtn, "Flags for the swiss round of the 2020 Standard Tournament");
            this.megmetBtn.UseVisualStyleBackColor = true;
            this.megmetBtn.Click += new System.EventHandler(this.swissFlags);
            // 
            // zoraBtn
            // 
            this.zoraBtn.Location = new System.Drawing.Point(191, 407);
            this.zoraBtn.Name = "zoraBtn";
            this.zoraBtn.Size = new System.Drawing.Size(75, 23);
            this.zoraBtn.TabIndex = 20;
            this.zoraBtn.Text = "Brackets";
            this.toolTip1.SetToolTip(this.zoraBtn, "Brackets flags for the 2020 Standard Tournament.");
            this.zoraBtn.UseVisualStyleBackColor = true;
            this.zoraBtn.Click += new System.EventHandler(this.bracketFlags);
            // 
            // wizzBtn
            // 
            this.wizzBtn.Location = new System.Drawing.Point(272, 407);
            this.wizzBtn.Name = "wizzBtn";
            this.wizzBtn.Size = new System.Drawing.Size(75, 23);
            this.wizzBtn.TabIndex = 21;
            this.wizzBtn.Text = "Finals";
            this.toolTip1.SetToolTip(this.wizzBtn, "Finals flags for the 2020 Standard Tournament");
            this.wizzBtn.UseVisualStyleBackColor = true;
            this.wizzBtn.Click += new System.EventHandler(this.finalsFlags);
            // 
            // paraBtn
            // 
            this.paraBtn.Location = new System.Drawing.Point(353, 407);
            this.paraBtn.Name = "paraBtn";
            this.paraBtn.Size = new System.Drawing.Size(75, 23);
            this.paraBtn.TabIndex = 22;
            this.paraBtn.Text = "Max Rando";
            this.toolTip1.SetToolTip(this.paraBtn, "If it can be randomized, it is.");
            this.paraBtn.UseVisualStyleBackColor = true;
            this.paraBtn.Click += new System.EventHandler(this.maxRandoFlags);
            // 
            // lizBtn
            // 
            this.lizBtn.Location = new System.Drawing.Point(434, 407);
            this.lizBtn.Name = "lizBtn";
            this.lizBtn.Size = new System.Drawing.Size(75, 23);
            this.lizBtn.TabIndex = 23;
            this.lizBtn.Text = "Four Gems";
            this.toolTip1.SetToolTip(this.lizBtn, "You only have to complete four palaces...but which ones should you pick?");
            this.lizBtn.UseVisualStyleBackColor = true;
            this.lizBtn.Click += new System.EventHandler(this.fourGemFlags);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(233, 61);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "Batch...";
            this.toolTip1.SetToolTip(this.button1, "Visit the website");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // customBox1
            // 
            this.customBox1.Location = new System.Drawing.Point(29, 470);
            this.customBox1.Name = "customBox1";
            this.customBox1.Size = new System.Drawing.Size(158, 20);
            this.customBox1.TabIndex = 25;
            this.toolTip1.SetToolTip(this.customBox1, "These flags represent the selected options. They can be copy/pasted.");
            // 
            // customBox2
            // 
            this.customBox2.Location = new System.Drawing.Point(191, 470);
            this.customBox2.Name = "customBox2";
            this.customBox2.Size = new System.Drawing.Size(158, 20);
            this.customBox2.TabIndex = 26;
            this.toolTip1.SetToolTip(this.customBox2, "These flags represent the selected options. They can be copy/pasted.");
            // 
            // customSave1
            // 
            this.customSave1.Location = new System.Drawing.Point(29, 496);
            this.customSave1.Name = "customSave1";
            this.customSave1.Size = new System.Drawing.Size(75, 23);
            this.customSave1.TabIndex = 29;
            this.customSave1.Text = "Save";
            this.toolTip1.SetToolTip(this.customSave1, "Saves the current flags to this custom slot");
            this.customSave1.UseVisualStyleBackColor = true;
            this.customSave1.Click += new System.EventHandler(this.customSave1_Click);
            // 
            // customLoad1
            // 
            this.customLoad1.Location = new System.Drawing.Point(111, 496);
            this.customLoad1.Name = "customLoad1";
            this.customLoad1.Size = new System.Drawing.Size(75, 23);
            this.customLoad1.TabIndex = 30;
            this.customLoad1.Text = "Load";
            this.toolTip1.SetToolTip(this.customLoad1, "Loads the flags in this current custom slot");
            this.customLoad1.UseVisualStyleBackColor = true;
            this.customLoad1.Click += new System.EventHandler(this.customLoad1_Click);
            // 
            // customBox3
            // 
            this.customBox3.Location = new System.Drawing.Point(353, 470);
            this.customBox3.Name = "customBox3";
            this.customBox3.Size = new System.Drawing.Size(158, 20);
            this.customBox3.TabIndex = 31;
            this.toolTip1.SetToolTip(this.customBox3, "These flags represent the selected options. They can be copy/pasted.");
            // 
            // customSave2
            // 
            this.customSave2.Location = new System.Drawing.Point(192, 496);
            this.customSave2.Name = "customSave2";
            this.customSave2.Size = new System.Drawing.Size(75, 23);
            this.customSave2.TabIndex = 33;
            this.customSave2.Text = "Save";
            this.toolTip1.SetToolTip(this.customSave2, "Saves the current flags to this custom slot");
            this.customSave2.UseVisualStyleBackColor = true;
            this.customSave2.Click += new System.EventHandler(this.customSave2_Click);
            // 
            // customLoad2
            // 
            this.customLoad2.Location = new System.Drawing.Point(273, 496);
            this.customLoad2.Name = "customLoad2";
            this.customLoad2.Size = new System.Drawing.Size(75, 23);
            this.customLoad2.TabIndex = 34;
            this.customLoad2.Text = "Load";
            this.toolTip1.SetToolTip(this.customLoad2, "Loads the flags in this current custom slot");
            this.customLoad2.UseVisualStyleBackColor = true;
            this.customLoad2.Click += new System.EventHandler(this.customLoad2_Click);
            // 
            // customSave3
            // 
            this.customSave3.Location = new System.Drawing.Point(353, 496);
            this.customSave3.Name = "customSave3";
            this.customSave3.Size = new System.Drawing.Size(75, 23);
            this.customSave3.TabIndex = 35;
            this.customSave3.Text = "Save";
            this.toolTip1.SetToolTip(this.customSave3, "Saves the current flags to this custom slot");
            this.customSave3.UseVisualStyleBackColor = true;
            this.customSave3.Click += new System.EventHandler(this.customSave3_Click);
            // 
            // customLoad3
            // 
            this.customLoad3.Location = new System.Drawing.Point(434, 496);
            this.customLoad3.Name = "customLoad3";
            this.customLoad3.Size = new System.Drawing.Size(75, 23);
            this.customLoad3.TabIndex = 36;
            this.customLoad3.Text = "Load";
            this.toolTip1.SetToolTip(this.customLoad3, "Loads the flags in this current custom slot");
            this.customLoad3.UseVisualStyleBackColor = true;
            this.customLoad3.Click += new System.EventHandler(this.customLoad3_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(27, 454);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(89, 13);
            this.label22.TabIndex = 27;
            this.label22.Text = "Custom Flags #1:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(189, 454);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(89, 13);
            this.label23.TabIndex = 28;
            this.label23.Text = "Custom Flags #2:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(351, 454);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(89, 13);
            this.label24.TabIndex = 32;
            this.label24.Text = "Custom Flags #3:";
            // 
            // label25
            // 
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Location = new System.Drawing.Point(29, 445);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(480, 1);
            this.label25.TabIndex = 26;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // customRooms
            // 
            this.customRooms.AutoSize = true;
            this.customRooms.Location = new System.Drawing.Point(6, 52);
            this.customRooms.Name = "customRooms";
            this.customRooms.Size = new System.Drawing.Size(151, 17);
            this.customRooms.TabIndex = 10;
            this.customRooms.Text = "Include Community Rooms";
            this.toolTip1.SetToolTip(this.customRooms, "When selected, rooms created by the Zelda 2 community will be included in the roo" +
        "m pool");
            this.customRooms.UseVisualStyleBackColor = true;
            // 
            // blockerBox
            // 
            this.blockerBox.AutoSize = true;
            this.blockerBox.Location = new System.Drawing.Point(6, 75);
            this.blockerBox.Name = "blockerBox";
            this.blockerBox.Size = new System.Drawing.Size(230, 17);
            this.blockerBox.TabIndex = 11;
            this.blockerBox.Text = "Blocking Rooms Can Appear in Any Palace";
            this.toolTip1.SetToolTip(this.blockerBox, "When selected, a palace can be blocked by any of the item/spell blocked rooms");
            this.blockerBox.UseVisualStyleBackColor = true;
            // 
            // bossRoomBox
            // 
            this.bossRoomBox.AutoSize = true;
            this.bossRoomBox.Location = new System.Drawing.Point(6, 98);
            this.bossRoomBox.Name = "bossRoomBox";
            this.bossRoomBox.Size = new System.Drawing.Size(153, 17);
            this.bossRoomBox.TabIndex = 12;
            this.bossRoomBox.Text = "Boss Rooms Exit to Palace";
            this.toolTip1.SetToolTip(this.bossRoomBox, "When selected, boss rooms will no longer lead outside, they will lead to more pal" +
        "ace");
            this.bossRoomBox.UseVisualStyleBackColor = true;
            // 
            // palaceBox
            // 
            this.palaceBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.palaceBox.FormattingEnabled = true;
            this.palaceBox.Items.AddRange(new object[] {
            "Vanilla",
            "Shuffled",
            "Reconstructed"});
            this.palaceBox.Location = new System.Drawing.Point(6, 25);
            this.palaceBox.Name = "palaceBox";
            this.palaceBox.Size = new System.Drawing.Size(151, 21);
            this.palaceBox.TabIndex = 13;
            this.toolTip1.SetToolTip(this.palaceBox, "Palace modes: Shuffle - same rooms different order; Reconstructed: any rooms from" +
        " the room pool can appear and palaces can change sizes.");
            this.palaceBox.SelectedIndexChanged += new System.EventHandler(this.palaceBox_SelectedIndexChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 10);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(69, 13);
            this.label43.TabIndex = 14;
            this.label43.Text = "Palace Style:";
            this.toolTip1.SetToolTip(this.label43, "Palace modes: Shuffle - same rooms different order; Reconstructed: any rooms from" +
        " the room pool can appear and palaces can change sizes.");
            // 
            // dashBox
            // 
            this.dashBox.AutoSize = true;
            this.dashBox.Location = new System.Drawing.Point(6, 122);
            this.dashBox.Name = "dashBox";
            this.dashBox.Size = new System.Drawing.Size(136, 17);
            this.dashBox.TabIndex = 21;
            this.dashBox.Text = "Replace Fire with Dash";
            this.toolTip1.SetToolTip(this.dashBox, "When selected, the Fire spell will be replaced with a Dash spell that makes Link " +
        "move faster");
            this.dashBox.UseVisualStyleBackColor = true;
            this.dashBox.CheckedChanged += new System.EventHandler(this.dashBox_CheckedChanged);
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 529);
            this.Controls.Add(this.customLoad3);
            this.Controls.Add(this.customSave3);
            this.Controls.Add(this.customLoad2);
            this.Controls.Add(this.customSave2);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.customBox3);
            this.Controls.Add(this.customLoad1);
            this.Controls.Add(this.customSave1);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.customBox2);
            this.Controls.Add(this.customBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lizBtn);
            this.Controls.Add(this.paraBtn);
            this.Controls.Add(this.wizzBtn);
            this.Controls.Add(this.zoraBtn);
            this.Controls.Add(this.megmetBtn);
            this.Controls.Add(this.botBtn);
            this.Controls.Add(this.wikiBtn);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.updateBtn);
            this.Controls.Add(this.flagBox);
            this.Controls.Add(this.generateBtn);
            this.Controls.Add(this.fileBtn);
            this.Controls.Add(this.seedBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.seedTextBox);
            this.Controls.Add(this.fileTextBox);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainUI";
            this.Text = "Zelda 2 Randomizer";
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.itemGrp.ResumeLayout(false);
            this.itemGrp.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.expBox.ResumeLayout(false);
            this.expBox.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox fileTextBox;
        private System.Windows.Forms.TextBox seedTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox itemGrp;
        private System.Windows.Forms.CheckBox keyBox;
        private System.Windows.Forms.CheckBox hammerBox;
        private System.Windows.Forms.CheckBox crossBox;
        private System.Windows.Forms.CheckBox fluteBox;
        private System.Windows.Forms.CheckBox bootsBox;
        private System.Windows.Forms.CheckBox raftBox;
        private System.Windows.Forms.CheckBox gloveBox;
        private System.Windows.Forms.CheckBox candleBox;
        private System.Windows.Forms.CheckBox shuffleItemBox;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox thunderBox;
        private System.Windows.Forms.CheckBox spellShuffleBox;
        private System.Windows.Forms.CheckBox shieldBox;
        private System.Windows.Forms.CheckBox jumpBox;
        private System.Windows.Forms.CheckBox lifeBox;
        private System.Windows.Forms.CheckBox fairyBox;
        private System.Windows.Forms.CheckBox fireBox;
        private System.Windows.Forms.CheckBox reflectBox;
        private System.Windows.Forms.CheckBox spellBox;
        private System.Windows.Forms.Button seedBtn;
        private System.Windows.Forms.Button fileBtn;
        private System.Windows.Forms.Button generateBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox heartCmbo;
        private System.Windows.Forms.CheckBox livesBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox techCmbo;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.CheckBox shuffleEnemyHPBox;
        private System.Windows.Forms.GroupBox expBox;
        private System.Windows.Forms.CheckBox lifeExpNeeded;
        private System.Windows.Forms.CheckBox magicExpNeeded;
        private System.Windows.Forms.CheckBox shuffleAtkExp;
        private System.Windows.Forms.CheckBox shuffleAllExp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox numGemsCbo;
        private System.Windows.Forms.CheckBox swordImmuneBox;
        private System.Windows.Forms.CheckBox stealExpAmt;
        private System.Windows.Forms.CheckBox stealExpBox;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox disableLowHealthBeep;
        private System.Windows.Forms.CheckBox tbirdBox;
        private System.Windows.Forms.CheckBox allowPathEnemies;
        private System.Windows.Forms.CheckBox shuffleEncounters;
        private System.Windows.Forms.CheckBox shufflePalaceEnemies;
        private System.Windows.Forms.CheckBox shuffleOverworldEnemies;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox jumpNormalbox;
        private System.Windows.Forms.TextBox flagBox;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox mixEnemies;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button wikiBtn;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.CheckBox mixItemBox;
        private System.Windows.Forms.CheckBox overworldItemBox;
        private System.Windows.Forms.CheckBox palaceItemBox;
        private System.Windows.Forms.CheckBox shuffleSmallItemsBox;
        private System.Windows.Forms.CheckBox palaceKeys;
        private System.Windows.Forms.CheckBox palacePalette;
        private System.Windows.Forms.Button lizBtn;
        private System.Windows.Forms.Button paraBtn;
        private System.Windows.Forms.Button wizzBtn;
        private System.Windows.Forms.Button zoraBtn;
        private System.Windows.Forms.Button megmetBtn;
        private System.Windows.Forms.Button botBtn;
        private System.Windows.Forms.CheckBox palaceSwapBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox upaBox;
        private System.Windows.Forms.CheckBox gpBox;
        private System.Windows.Forms.CheckBox fastSpellBox;
        private System.Windows.Forms.CheckBox kasutoBox;
        private System.Windows.Forms.CheckBox removeTbird;
        private System.Windows.Forms.CheckBox pbagItemShuffleBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox maxHeartsBox;
        private System.Windows.Forms.CheckBox beamBox;
        private System.Windows.Forms.CheckBox p7Shuffle;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox shuffleDripper;
        private System.Windows.Forms.CheckBox enemyPalette;
        private System.Windows.Forms.ComboBox hpCmbo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox disableMusicBox;
        private System.Windows.Forms.ComboBox hideKasutoBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox spriteCmbo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox tunicColor;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox shieldColor;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox spellItemBox;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.CheckBox largeKey;
        private System.Windows.Forms.CheckBox large1up;
        private System.Windows.Forms.CheckBox large500;
        private System.Windows.Forms.CheckBox large200;
        private System.Windows.Forms.CheckBox large100;
        private System.Windows.Forms.CheckBox large50;
        private System.Windows.Forms.CheckBox largeRedJar;
        private System.Windows.Forms.CheckBox largeBlueJar;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox smallKey;
        private System.Windows.Forms.CheckBox small1up;
        private System.Windows.Forms.CheckBox small500;
        private System.Windows.Forms.CheckBox small200;
        private System.Windows.Forms.CheckBox small100;
        private System.Windows.Forms.CheckBox small50;
        private System.Windows.Forms.CheckBox smallRedJar;
        private System.Windows.Forms.CheckBox smallBlueJar;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox enemyDropBox;
        private System.Windows.Forms.CheckBox pbagDrop;
        private System.Windows.Forms.TextBox customBox1;
        private System.Windows.Forms.TextBox customBox2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button customSave1;
        private System.Windows.Forms.Button customLoad1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox customBox3;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button customSave2;
        private System.Windows.Forms.Button customLoad2;
        private System.Windows.Forms.Button customSave3;
        private System.Windows.Forms.Button customLoad3;
        private System.Windows.Forms.ComboBox beamCmbo;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox standardDrops;
        private System.Windows.Forms.CheckBox randoDrops;
        private System.Windows.Forms.CheckBox shufflePbagExp;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.ComboBox lifeCapBox;
        private System.Windows.Forms.ComboBox magCapBox;
        private System.Windows.Forms.ComboBox atkCapBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox combineFireBox;
        private System.Windows.Forms.CheckBox disableJarBox;
        private System.Windows.Forms.CheckBox shuffleSpellLocationsBox;
        private System.Windows.Forms.CheckBox lifeRefilBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox scaleLevels;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.CheckBox townSpellHints;
        private System.Windows.Forms.CheckBox spellItemHints;
        private System.Windows.Forms.CheckBox communityBox;
        private System.Windows.Forms.CheckBox helpfulHints;
        private System.Windows.Forms.ComboBox encounterBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox atkEffBox;
        private System.Windows.Forms.ComboBox magEffBox;
        private System.Windows.Forms.ComboBox lifeEffBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox expDropBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox startLifeBox;
        private System.Windows.Forms.ComboBox startMagBox;
        private System.Windows.Forms.ComboBox startAtkBox;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox continentConnectionBox;
        private System.Windows.Forms.CheckBox upAC1;
        private System.Windows.Forms.CheckBox saneCaveShuffleBox;
        private System.Windows.Forms.CheckBox hideLocsBox;
        private System.Windows.Forms.CheckBox boulderConnectionBox;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox eastBiome;
        private System.Windows.Forms.ComboBox dmBiome;
        private System.Windows.Forms.ComboBox westBiome;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.CheckBox flashingOff;
        private System.Windows.Forms.ComboBox mazeBiome;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.CheckBox vanillaOriginalTerrain;
        private System.Windows.Forms.CheckBox shuffleHidden;
        private System.Windows.Forms.CheckBox bossItem;
        private System.Windows.Forms.CheckBox waterBoots;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox spellEnemy;
        private System.Windows.Forms.CheckBox baguBox;
        private System.Windows.Forms.CheckBox customRooms;
        private System.Windows.Forms.CheckBox blockerBox;
        private System.Windows.Forms.CheckBox bossRoomBox;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox palaceBox;
        private System.Windows.Forms.CheckBox dashBox;
    }
}

